// L00 E01
// Gabrele Iurlaro

#include <stdio.h>

#define N 31
void stampaVettore(int v[], int n, int s, int e){
    int i;
    for(i = s; i<e; i++)
        printf("%d ", v[i]);
    printf("\n");
    return;
}

int main() {
    int v[N];
    int l=0,ls=0,i=0,j=0,cnt=0, end = 0;
    while(i<N &&  end == 0){
        scanf("%d", &v[i]);
        if(v[i] == -1)
            end = 1;
        i++;
    }
    l = i-1;
    stampaVettore(v,l,0,l);
    for(i=0; i < l; i++){
        if(v[i]!=0){
            cnt++;
        }
        else{
            if(cnt > ls)
                ls = cnt;
            cnt = 0;
        }
    }
    printf("%d\n", ls);
    for(i=0; i<l; i++){
        if(v[i]!=0)
            j++;
        else {
            if (j == ls)
                stampaVettore(v, ls, i - ls, i);
            j = 0;
        }
    }
    return 0;
}
