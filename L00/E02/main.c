// L00 E02
// Gabriele Iurlaro

#include <stdio.h>
#include <string.h>

int conta(char s[], int n);

int main() {
    FILE *fp;
    int n,N,i,ss=0;
    char parola[21], nomeFile[21];
    scanf("%s",nomeFile);
    printf("> ");
    scanf("%d", &n);
    fp = fopen(nomeFile, "r");
    fscanf(fp,"%d",&N);
    for(i = 0; i<N; i++){
        fscanf(fp,"%s",parola);
        ss = ss + conta(parola,n);
    }
    printf("%d", ss);
    return 0;
}

int conta(char s[], int n){
    int count=0,i,j,vocali;
    for(i=0; i < strlen(s)-n-1; i++){
        for(j=i; j<i+n; j++)
            if(s[j] == 'a' || s[j] == 'e' || s[j] == 'i' || s[j] == 'o' || s[j] == 'u')
                vocali++;
        if(vocali >= 2){
            count++;
            vocali = 0;
        }
    }
    return count;
}
