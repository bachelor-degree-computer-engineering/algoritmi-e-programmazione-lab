//       L00 E03
//   Gabriele Iurlaro

#include <stdio.h>
#define maxN 30

void ruota(int v[maxN],int N, int P, int dir);
void stampa(int v[maxN], int l);

int main() {
    int v[maxN], i, n, p, dir,continua = 1;
    printf("Inserisci N > ");
    scanf("%d", &n);
    for(i=0; i<n; i++){
        scanf("%d",&v[i]);
    }
    stampa(v,n);
    while(continua){
        scanf("%d%d",&p,&dir);
        if(!p)
            break;
        ruota(v,n,p,dir);
        stampa(v,n);
    }
    return 0;
}

void ruota(int v[maxN], int N, int P, int dir){
    int tmp[N];  //effettuo una rotazione attraverso un mapping del vettore in un vettore temporaneo
    int i,pos;
    for(i=0; i<N; i++){
       pos = (i-dir*P)%N;
       if(pos < 0)
           pos = N +pos;
       tmp[pos]=v[i];
    }
    for(i=0; i<N; i++)
        v[i]=tmp[i];
    return;
}

void stampa(int v[maxN], int l){
    for(int i=0; i<l; i++)
        printf("%d ",v[i]);
    printf("\n");
    return;
}
