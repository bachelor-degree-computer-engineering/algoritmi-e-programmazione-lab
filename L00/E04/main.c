//       L00 E04
//   Gabriele Iurlaro

#include <stdio.h>
#define MAX 20

int minimo(int a, int b);
void stampaMatrice(int matrice[MAX][MAX],int r,int c, int sr,int sc);
int sommaElementi(int matrice[MAX][MAX],int r,int c, int sr,int sc);

int main() {
    FILE *fp;
    int nr,nc,i,j,mat[MAX][MAX],xmax,ymax,continua=1,n,s=0,smax=0;
    fp = fopen("file.txt", "r");
    fscanf(fp,"%d %d",&nr,&nc);
    for(i=0; i < nc; i++)
        for(j=0; j<nc; j++)
            fscanf(fp,"%d",&mat[i][j]);
    stampaMatrice(mat,nr,nc,0,0);
    while(continua){
        smax = 0;
        printf("> ");
        scanf("%d",&n);
        if(n < 1 || n > minimo(nr,nc))
            break;
        for (i = 0; i <= nr-n ; i++) {
            for (j = 0;j<=nc-n; j++) {
                stampaMatrice(mat,n,n,i,j);
                s = sommaElementi(mat,n,n,i,j);
                if(s > smax){
                    smax = s;
                    xmax = i;
                    ymax = j;
                }
            }
            printf("\n\n");
        }
        printf("Matrice di somma massima.\nsomma=%d\n\n",smax);
        stampaMatrice(mat,n,n,xmax,ymax);
    }
    return 0;
}

int minimo(int a, int b){
    if(a < b)
        return a;
    else
        return b;
}

void stampaMatrice(int matrice[MAX][MAX],int r,int c, int sr,int sc){
    int i,j;
    for(i=sr; i < sr+r; i++){
        for (int j = sc; j < sc+c; j++)
            printf("%2d ",matrice[i][j]);
        printf("\n");
    }
}
int sommaElementi(int matrice[MAX][MAX],int r,int c, int sr,int sc){
    int somma = 0,i,j;
    for(i=sr; i < sr+r; i++) {
        for (int j = sc; j < sc + c; j++)
            somma = somma + matrice[i][j];
        return somma;
    }
}
