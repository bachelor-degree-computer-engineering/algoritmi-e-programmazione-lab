//       L01 E01
//   Gabriele Iurlaro

#include <stdio.h>

int max(int v[],int n);
void leggiMatrice(FILE *fp, int matrice[20][20],int n,int m);

int main() {
    FILE *fp;
    int n, m, i, j, squadra, matrice[20][20], punti[20];
    fp = fopen("prova.txt", "r");
    fscanf(fp, "%d %d", &n, &m);
    leggiMatrice(fp, matrice, n, m);
    for(i=0;i<n;i++) punti[i]=0;
    for(i=0; i<m; i++){
        for (j=0;  j<n ; j++) {
            punti[i] += matrice[j][i];
        }
        squadra = max(punti,n);
        printf("Nella giornata %d ha vinto la squadra %d\n",i,squadra);
    }
    fclose(fp);
    return 0;
}

int max(int v[],int n){
    int i,pos,massimo=0;
    for(i=0; i<n; i++)
        if(v[i]>massimo){
            massimo = v[i];
            pos = i;
        }
    return pos;
}

void leggiMatrice(FILE *fp, int matrice[20][20],int n,int m){
    int i,j;
    for(i=0; i <n; i++)
        for(j=0; j<m; j++)
            fscanf(fp,"%d", &matrice[i][j]);
    return;
}
