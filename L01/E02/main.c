//       L01 E01
//   Gabriele Iurlaro

#include <stdio.h>
#include <string.h>
#include <ctype.h>

typedef struct{
    char src[21];
    char k[21];         // vedo la ricodifica nella forma $%d$
}dizionario;

void riempiDizionario(FILE *fp, dizionario dizionario[], int S);

int main() {
    FILE *fin,*fout;
    int i=0,j=0,s,t=0,k,flag,found,lenFound;
    dizionario diz[30];
    char riga[201];
    char str[201];
    fin = fopen("dizionario.txt","r");
    fscanf(fin,"%d\n",&s);
    riempiDizionario(fin, diz, s);
    printf("Dizionario\n");
    for(i=0; i<s; i++)
        printf("%s %s\n",diz[i].k, diz[i].src);
    fclose(fin);
    fin = fopen("sorgente.txt", "r");
    fout = fopen("ricodifica.txt","w");
    while(fgets(riga,200,fin)!=NULL){
        i=0;
        while(riga[i] != '\0'){
            found = -1;
            lenFound = 0;
            for(j=0; j<s; j++){
                k=0;
                flag =1;
                while(diz[j].src[k]!='\0' && flag){
                    if(diz[j].src[k] != riga[i+k]) flag = 0;
                    k++;
                }
                if(flag == 1 && k > lenFound){
                    found = j;
                    lenFound = k;
                }
            }
            if(found == -1){
                str[t++] = riga[i++];
            }else{
                s = 0;
                while (diz[found].k[s] != '\0') {
                    str[t++] = diz[found].k[s++];
                }
                i += lenFound;
            }
        }
        str[t] = '\0';
        fprintf(fout, "%s",str);
        t = 0;
        str[t] = '\0';
    }
    fclose(fin);
    fclose(fout);
    return 0;
}

void riempiDizionario(FILE *fp,dizionario dizionario[],int S){
    int i;
    for(i=0; i<S; i++){
        fscanf(fp,"%s %s\n",dizionario[i].k, dizionario[i].src);
    }
    return;
}