//       L01 E03
//   Gabriele Iurlaro

#define MAX 30

#include <stdio.h>
#include <string.h>

void ruota(int matrice[MAX][MAX], int nr, int nc, int dir, int verso, int indice, int P);
void stampaMatrice(int matrice[MAX][MAX], int n, int m);

int main() {
    FILE *fp;
    char nomeFile[21], selettore[8], direzione[9];
    int matrice[MAX][MAX];
    int i,j,k,p, n,m,dir=0,verso=-1;
    printf("Nome file> ");
    fscanf(stdin,"%s",nomeFile);
    fp  = fopen(nomeFile, "r");
    if(fp == NULL)
        return 1;
    fscanf(fp,"%d %d", &n,&m);
    for(i=0; i<n; i++)
        for(j=0; j<m; j++)
            fscanf(fp,"%d", &matrice[i][j]);
    fclose(fp);
    scanf("%s %d %s %d",selettore, &k, direzione, &p);
    while(strcmp(selettore,"fine") != 0){
        stampaMatrice(matrice,n,m);
        if(strcmp(selettore,"riga") == 0)
            verso =1;
        else if(strcmp(selettore, "colonna")==0)
            verso = 0;
        if(strcmp(direzione,"destra") == 0 || strcmp(direzione,"giu") == 0)
            dir = -1;
        else if(strcmp(direzione,"sinistra") == 0 || strcmp(direzione,"sopra") == 0)
            dir = 1;
        printf("direzione: %d\n", dir);
        ruota(matrice,n,m,dir,verso,k,p);
        stampaMatrice(matrice,n,m);
        scanf("%s %d %s %d",selettore, &k, direzione, &p);
    }
    return 0;
}

void ruota(int matrice[MAX][MAX], int nr, int nc, int dir, int verso,int indice, int P){
    int tmp[MAX], ruotato[MAX];
    int i,pos=0;
    indice--;
    if(verso == 1){
        for(i=0; i<nc; i++)
            tmp[i] = matrice[indice][i];
        for(i=0; i<nc; i++) {
            pos = (i - dir * P) % nc;
            if (pos < 0)
                pos = nc + pos;
            ruotato[pos] = tmp[i];
        }
        for(i=0; i<nc; i++)
             matrice[indice][i] = ruotato[i];
    }
    else if(verso == 0){
        for(i=0; i<nr; i++)
            tmp[i] = matrice[i][indice];
        for(i=0; i<nr; i++) {
            pos = (i - dir * P) % nr;
            if (pos < 0)
                pos = nr + pos;
            ruotato[pos] = tmp[i];
        }
        for(i=0; i<nc; i++)
            matrice[i][indice] = ruotato[i];
    }
    return;
}

void stampaMatrice(int matrice[MAX][MAX], int n, int m){
    int i,j;
    for(i=0; i<n; i++){
        for(j=0; j<m; j++)
            printf("%2d ", matrice[i][j]);
        printf("\n");
    }
    printf("\n\n");
    return;
}

