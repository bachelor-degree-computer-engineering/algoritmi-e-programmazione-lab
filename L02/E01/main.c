#include <stdio.h>
#define MAX 50

typedef struct{
    int r;
    int c;
}point;

typedef struct{
    point p;
    int b;
    int h;
    int trovato;
}rett;

void leggiMatrice(FILE *fp, int matrice[MAX][MAX],int n, int m);
void stampaMatrice(int matrice[MAX][MAX],int n, int m);
rett riconosciRett(int matrice[MAX][MAX], point sp, int nr, int nc);

int main(){
    int nr,nc,mappa[MAX][MAX],i,j,rtot=0,apos=0,hpos=0,bpos=0,maxa=0,maxb=0,maxh=0;
    rett rettangoli[MAX],rtt;
    point sp;
    FILE *in;
    in = fopen("mappa.txt","r");
    fscanf(in,"%d %d",&nr,&nc);
    leggiMatrice(in,mappa,nr,nc);
    stampaMatrice(mappa,nr,nc);
    fclose(in);
    for(i=0; i<nr; i++)
        for (j=0;j<nc; j++) {
            if(mappa[i][j] == 1) {
                sp.r = i;
                sp.c = j;
                rtt = riconosciRett(mappa, sp, nr, nc);
                if (rtt.trovato) {
                    rettangoli[rtot++] = rtt;
                    //printf("Trovato rettangolo:<%d,%d> b=%d, h=%d\n", rtt.p.r, rtt.p.c, rtt.b, rtt.h);
                    //stampaMatrice(mappa, nr, nc);
                }
            }
        }
    for(i=0; i<rtot; i++){
        if(rettangoli[i].b > maxb){
            maxb = rettangoli[i].b;
            bpos = i;
        }
        if(rettangoli[i].h > maxh){
            maxh = rettangoli[i].h;
            hpos = i;
        }
        if(rettangoli[i].h*rettangoli[i].b > maxh) {
            maxa = rettangoli[i].b * rettangoli[i].h;
            apos = i;
        }
    }
    printf("Max Base: estr. sup. SX=<%d,%d> b=%d, h=%d, Area=%d\n",rettangoli[bpos].p.r,rettangoli[bpos].p.c,maxb,rettangoli[bpos].h,rettangoli[bpos].h*maxb);
    printf("Max altezza: estr. sup. SX=<%d,%d> b=%d, h=%d, Area=%d\n",rettangoli[hpos].p.r,rettangoli[hpos].p.c,rettangoli[hpos].b,maxh,rettangoli[hpos].b*maxh);
    printf("Max area: estr. sup. SX=<%d,%d> b=%d, h=%d, Area=%d\n",rettangoli[apos].p.r,rettangoli[apos].p.c,rettangoli[apos].b,rettangoli[apos].h,maxa);
    return 0;
}

void leggiMatrice(FILE *fp, int matrice[MAX][MAX],int n, int m){
    int i,j;
    for(i=0; i <n; i++)
        for (j=0; j<m; j++)
            fscanf(fp, "%d",&matrice[i][j]);
    return;
}

void stampaMatrice(int matrice[MAX][MAX], int n, int m){
    int i,j;
    for(i=0; i<n; i++){
        for(j=0; j<m; j++)
            printf("%2d ", matrice[i][j]);
        printf("\n");
    }
    return;
}

rett riconosciRett(int matrice[MAX][MAX], point sp, int nr, int nc){
    rett rtt;
    int i = sp.r,j=sp.c;
    rtt.p = sp;
    for(;matrice[i][j]==1 && i < nr; i++) {
        for (; matrice[i][j] == 1 && j < nc; j++) {
            matrice[i][j] = -1;
        }
        rtt.b = j;
        j = sp.c;
    }
    rtt.b = rtt.b-sp.c;
    rtt.h = i-sp.r;
    if(rtt.b > 0 && rtt.h > 0)
        rtt.trovato = 1;
    return rtt;
}


