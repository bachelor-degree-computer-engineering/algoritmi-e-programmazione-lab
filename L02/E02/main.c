#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define MAXS 30
#define NMAX 1000

typedef struct {
    char cod[MAXS+1];
    char partenza[MAXS+1];
    char dest[MAXS+1];
    char data[11];
    char ora_p[6];
    char ora_a[6];
    int ritardo;
}t_corse;

typedef enum{
    r_date, r_partenza, r_capolinea, r_ritardo, r_ritardo_tot, r_fine
}e_comando;

e_comando leggiComando();
int selezionaDati(t_corse v[], int dim, e_comando cmd);
void strlower(char stringa[]);
int data_to_int(char stringa[11]);
void stampa(t_corse t);
void f_date(t_corse v[], int dim);
void f_partenza(t_corse v[], int dim);
void f_capolinea(t_corse v[], int dim);
void f_ritardo(t_corse v[],int dim);
void f_ritardo_tot(t_corse v[], int dim);

int main(){
    t_corse corse[NMAX];
    int i,n,continua;
    e_comando cmd;
    FILE *in;
    in = fopen("corse.txt","r");
    fscanf(in, "%d", &n);
    for(i=0; i<n; i++){
        fscanf(in,"%s %s %s %s %s %s %d",corse[i].cod,corse[i].partenza,corse[i].dest,corse[i].data,corse[i].ora_p,corse[i].ora_a,&corse[i].ritardo);
    }
    for(i=0; i<n;i++) stampa(corse[i]);
    continua = 1;
    while(continua){
        cmd = leggiComando();
        continua = selezionaDati(corse,n,cmd);
    }
    return 0;
}

e_comando leggiComando(){
    e_comando cmd;
    char stringa[13];
    printf("> ");
    scanf("%s", stringa);
    strlower(stringa);
    if(strcmp(stringa,"date")==0)
        cmd = r_date;
    if(strcmp(stringa,"partenza")==0)
        cmd = r_partenza;
    if(strcmp(stringa,"capolinea")==0)
        cmd = r_capolinea;
    if(strcmp(stringa,"ritardo")==0)
        cmd = r_ritardo;
    if(strcmp(stringa,"ritardo_tot")==0)
        cmd = r_ritardo_tot;
    if(strcmp(stringa,"fine")==0)
        cmd = r_fine;
    return cmd;
}

int selezionaDati(t_corse v[], int dim, e_comando cmd){
    switch(cmd){
        case r_date:
            f_date(v,dim);
            break;
        case r_partenza:
            f_partenza(v,dim);
            break;
        case r_capolinea:
            f_capolinea(v,dim);
            break;
        case r_ritardo:
            f_ritardo(v,dim);
            break;
        case r_ritardo_tot:
            f_ritardo_tot(v,dim);
            break;
        case r_fine:
           return 0;
    }
    return 1;
}

void strlower(char stringa[]){
    int i;
    for(i=0; stringa[i] != 0; i++) stringa[i] = tolower(stringa[i]);
    return;
}

void stampa(t_corse t){
    printf("%s %s %s %s %s %s %d\n",t.cod,t.partenza,t.dest,t.data,t.ora_p,t.ora_a,t.ritardo);
}

int data_to_int(char stringa[11]){
    int a,m,g;
    sscanf(stringa,"%d/%d/%d",&a,&m,&g);
    return 100*a + 30*m + g;
}

void f_date(t_corse v[],int dim){
    char data1[11],data2[11];
    int d1,d2;
    int i;
    printf("Inserire due date> ");
    scanf("%s %s",data1,data2);
    d1 = data_to_int(data1); d2 = data_to_int(data2);
    printf("%d %d datab:%d",d1,d2,data_to_int(v[i].data));
    for(i=0; i<dim; i++){
        if(data_to_int(v[i].data) > d1 && data_to_int(v[i].data) < d2)
            stampa(v[i]);
    }
    return;
}

void f_partenza(t_corse v[], int dim){
    int i;
    char partenza[MAXS];
    printf("Inserisci una partenza> ");
    scanf("%s",partenza);
    for (i = 0;i<dim;i++)
        if(strcmp(partenza,v[i].partenza)==0)
            stampa(v[i]);
    return;
}

void f_capolinea(t_corse v[], int dim){
    int i;
    char capolinea[MAXS];
    printf("Inserisci una partenza> ");
    scanf("%s",capolinea);
    for (i = 0;i<dim;i++)
        if(strcmp(capolinea,v[i].dest)==0)
            stampa(v[i]);
}

void f_ritardo(t_corse v[], int dim){
    char data1[11],data2[11];
    int d1,d2,i;
    printf("Inserire due date> ");
    scanf("%s %s",data1,data2);
    d1 = data_to_int(data1); d2 = data_to_int(data2);
    printf("%d %d",d1,d2);
    for(i=0; i<dim; i++){
        if(data_to_int(v[i].data) > d1 && data_to_int(v[i].data) < d2 && v[i].ritardo >0)
            stampa(v[i]);
    }
    return;
}

void f_ritardo_tot(t_corse v[], int dim){
    int i,ritardotot=0;
    char cod[MAXS];
    scanf("%s",cod);
    for (i = 0; i <dim; i++) {
        if(strcmp(v[i].cod,cod)==0)
            ritardotot += v[i].ritardo;
    }
    printf("%s %d\n",cod,ritardotot);
}