#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define MAX_RIGHE 200
#define MAX_PAROLA 25
#define MAX_OCCORRENZE 10
#define MAX_SEQUENZE 20

typedef struct{
    char parola[MAX_PAROLA+1];
    int pos;
}t_occorrenze;

void occorrenze(t_occorrenze matrice[MAX_SEQUENZE][MAX_OCCORRENZE], char sequenze[MAX_SEQUENZE][6],int ns, int k[MAX_SEQUENZE], char parola[MAX_PAROLA+1],int pos);

int main() {
    t_occorrenze moccorrenze[MAX_SEQUENZE][MAX_OCCORRENZE];
    char riga[MAX_RIGHE+1],parola[MAX_PAROLA+1], sequenze[MAX_SEQUENZE][6];
    int ns,i,j,nriga,nparole=0,k[MAX_SEQUENZE];
    FILE *in;
    in = fopen("sequenze.txt","r");
    for(i=0; i< MAX_SEQUENZE; i++) k[i]=0;
    fscanf(in,"%d", &ns);
    for(i=0; i <ns; i++)
        fscanf(in,"%s",sequenze[i]);
    fclose(in);
    in = fopen("testo.txt","r");
    while(fgets(riga,MAX_RIGHE,in) != NULL) {
        nriga = strlen(riga);
        i = 0;
        while (i < nriga) {
            j=0;
            while(ispunct(riga[i])==0 && isspace(riga[i])==0){
                parola[j++] = tolower(riga[i++]);
            }
            i++;
            parola[j] = '\0';
            if(strlen(parola)>0) nparole++;
            //printf("%s %d\n",parola,nparole);
            occorrenze(moccorrenze,sequenze,ns,k,parola,nparole);
        }
    }
    // stampo le occorrenze
    for (i=0; i<ns; i++) {
        printf("La sequenza %s è contenuta nel testo in:\n",sequenze[i]);
        for (j=0; j<k[i]; j++) {
            printf("- %s(parola %d nel testo)\n",moccorrenze[i][j].parola,moccorrenze[i][j].pos);
        }
        printf("\n");
    }
}

void occorrenze(t_occorrenze matrice[MAX_SEQUENZE][MAX_OCCORRENZE], char sequenze[MAX_SEQUENZE][6],int ns, int k[MAX_SEQUENZE], char parola[MAX_PAROLA+1],int pos){
    int i;
    for(i=0; i<ns; i++){
        if(strstr(parola,sequenze[i])!=NULL){
            strcpy(matrice[i][k[i]].parola, parola);
            matrice[i][k[i]].pos = pos;
            k[i]++;
        }
    }
    return;
}


