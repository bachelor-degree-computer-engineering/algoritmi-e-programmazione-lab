#include <stdio.h>
#define MAX 1000

void copia(int x[MAX],int y[MAX], int n);
void stampa(int v[], int n);
void selectionSort(int v[MAX], int n);
void insertionSort(int v[MAX],int n);
void shellSort(int v[MAX],int n);

int main(){
    int v[MAX], y[MAX], s,i,j,n;
    FILE *in;
    in = fopen("sort.txt", "r");
    fscanf(in, "%d", &s);
    for(i=0; i<s; i++){
        fscanf(in,"%d",&n);
        for(j=0; j<n; j++)
            fscanf(in, "%d", &v[j]);
        copia(v,y,n);
        selectionSort(y,n);
        copia(v,y,n);
        insertionSort(y,n);
        copia(v,y,n);
        shellSort(y,n);
    }
    fclose(in);
    return 0;
}

void copia(int x[MAX],int y[MAX], int n){
    int i;
    for(i=0; i<n; i++)
        y[i] = x[i];
    return;
}

void stampa(int v[], int n){
    int i;
    for(i=0; i<n; i++)
        printf("%d ",v[i]);
    return;
}
void selectionSort(int v[MAX], int n){
    int i,j,tmp,min,s_cnt=0, ext=0, in = 0, tot = 0;
    printf("SELECTION SORT\n\nv = ");
    stampa(v,n);
    printf("\n");
    for(i=0; i<n; i++){
        min = i;
        ext++;
        in = 0;
        for(j=i+1;j<n;j++) {
            if (v[j] < v[min])
                min = j;
            in++;
            tot++;
        }
        printf("-Iterazione esterna %d ciclo interno %d\n",i,in);
        if(min != i){
            tmp = v[i];
            v[i] = v[min];
            v[min] = tmp;
            s_cnt++;
        }
    }
    printf("\n-Scambi effettuati: %d\n-Ciclo esterno %d\n-iterazioni totali %d\n\nv ordinato = ",s_cnt,ext,tot);
    stampa(v,n);
    printf("\n\n");
    return;
}

void insertionSort(int v[MAX],int n){
    int i,j,tmp,s_cnt=0, ext=0, in = 0, tot = 0;
    printf("INSERTION SORT\n\nv = ");
    stampa(v,n);
    printf("\n");
    for(i=1; i<n; i++){
        tmp = v[i];
        j = i-1;
        in=0; ext++;
        while(j >= 0 && tmp <v[j]){
            v[j+1] = v[j];
            s_cnt++; tot++; in++;
            j--;
        }
        printf("-Iterazione esterna %d cico interno %d\n",i,in);
        v[j+1]=tmp;
    }
    printf("\n-Scambi effettuati: %d\n-Ciclo esterno %d\n-iterazioni totali %d\n\nv ordinato = ",s_cnt,ext,tot);
    stampa(v,n);
    printf("\n\n");
    return;
}
void shellSort(int v[MAX],int n){
    int i,j,h=1,tmp,s_cnt=0, ext=0, in = 0, tot = 0;
    printf("SHELL SORT\n\nv = ");
    stampa(v,n);
    printf("\n\n");
    while(h < n/3)
        h = 3*h+1;
    while(h>=1){
        for (i = h; i < n; i++) {
            j = i;
            tmp = v[i];
            while(j >= h && tmp <= v[j-h]){
                v[j] = v[j-h];
                j -= h;
                s_cnt++; tot++;
            }
            v[j] = tmp;
        }
        h = h/3;
    }
    stampa(v,n);
    return;
}