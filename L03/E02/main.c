#include <stdio.h>

int isBigEndian();
void stampaCodifica(void* p,int size,int bigEndian);

int main(){
    float af;
    double ad;
    long double ald;
    int bigEndian;
    printf("float: %ld byte;\ndouble: %ld byte;\nlong double: %ld byte;\n", sizeof(af),sizeof(ad), sizeof(ald));
    printf("%s",isBigEndian()==1?"Codifica: Big endian":"Codifica: little Endian");
    bigEndian = isBigEndian();
    printf("\nInserisci un numero> ");
    scanf("%f",&af);
    scanf("%lf",&ad);
    scanf("%Lf",&ald);
    stampaCodifica((void*)&af, sizeof(af), bigEndian); printf("\n");
    stampaCodifica((void*)&ad, sizeof(ad), bigEndian); printf("\n");
    stampaCodifica((void*)&ald, sizeof(ald), bigEndian);
    return 0;
}

int isBigEndian(){
    unsigned int x = 1;
    char *c = (char*)&x;
    if((int) *c == 0) return 1;
    return 0;
}

void stampaCodifica(void* p, int size, int bigEndian){
    int i, k;
    char *c = (char*) p;
    if( size ==  16) size = 10;
    if (bigEndian) {
        for ( k = 0; k < size; k++ )
            for ( i = 8; i > 0; i-- ) {
                printf( "%d", (c[k]>>i)&1 );
                switch (size) {
                    case 4:
                        if ( k == (size)-1 && i == 7 ) printf("\n");
                        if ( k == (size)-2 && i == 7 ) printf("\n");
                        break;
                    case 8:
                        if ( k == (size)-1 && i == 7 ) printf("\n");
                        if ( k == (size)-2 && i == 4 ) printf("\n");
                        break;
                    case 10:
                        if ( k == (size)-1 && i == 7 ) printf("\n");
                        if ( k == (size)-2 && i == 0 ) printf("\n");
                        break;
                }
            }
    } else {
        for ( k = (size)-1; k >= 0; k-- )
            for ( i = 7; i >= 0; i-- ) {
                printf( "%d", (c[k]>>i)&1 );
                switch (size) {
                    case 4:
                        if ( k == (size)-1 && i == 7 ) printf("\n");
                        if ( k == (size)-2 && i == 7 ) printf("\n");
                        break;
                    case 8:
                        if ( k == (size)-1 && i == 7 ) printf("\n");
                        if ( k == (size)-2 && i == 4 ) printf("\n");
                        break;
                    case 10:
                        if ( k == (size)-1 && i == 7 ) printf("\n");
                        if ( k == (size)-2 && i == 0 ) printf("\n");
                        break;
                }
            }
    }
    printf("\n\n");
}