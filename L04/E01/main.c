#include <stdio.h>

int gcd(int a, int b);

int main(){
    int a,b;
    printf("Inserici due interi>");
    scanf("%d %d",&a,&b);
    printf("gcd(%d,%d) = %d",a,b,gcd(a,b));
    return 0;
}

int gcd(int a, int b){
    if(a==b)
        return a;
    if(a < b)
        return gcd(b,a);
    if(a%2 == 0 && b%2 == 0)
        return 2*gcd(a/2,b/2);
    if(a%2 != 0 && b%2 == 0)
        return gcd(a,b/2);
    if(a%2 == 0 && b%2 !=0)
        return gcd(a/2,b);
    if(a%2 != 0 && b%2 != 0)
        return gcd((a-b)/2, b);
}
