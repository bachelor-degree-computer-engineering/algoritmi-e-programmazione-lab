#include <stdio.h>

int majority(int *a, int N);
int majorityR(int *a, int N, int l, int r);

int main(){
    int v[10] = {1,1,2,1,1,1, 1,4,1,1};
    printf("Elemento maggioritario è %d",majority(v,10));
    return 0;
}


int majority(int *a, int N){
    return majorityR(a,N,0,N-1);
}

int majorityR(int *a, int N, int l, int r){
    int c,i,sx,dx,count=0;
    c = (l+r)/2;
    if(l >= r)
        return *a;
    sx = majorityR(a,N/2,l,c);
    for(i=l; i<=c; i++)
        if(a[i] == sx)
            count++;
        if(count >= N/2)
            return sx;
    dx = majorityR(a,N/2,c+1,r);
    for(i=c+1;i<=r; i++)
        if(a[i] == dx)
            count++;
        if(count >= N/2)
            return dx;
    return -1;
}
