#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define MAXS 30
#define NMAX 1000
int ord = 0;
typedef struct {
    char cod[MAXS+1];
    char partenza[MAXS+1];
    char dest[MAXS+1];
    char data[11];
    char ora_p[9];
    char ora_a[9];
    int gdata;
    int ora;
    int ritardo;
}t_corse;

typedef enum{
    r_stampa, r_date, r_partenza, r_capolinea, r_ritardo, r_ritardo_tot, r_fine, r_ordina, cod_tratta, r_ricerca
}e_comando;


e_comando leggiComando();
int selezionaDati(t_corse v[], int dim, e_comando cmd);
void strlower(char stringa[]);
int data_to_int(char stringa[11]);
int ora_to_int(char stringa[11]);
void f_stampa(FILE* in, t_corse *t,int dim);
void stampa(FILE *in, t_corse t);
void f_date(t_corse v[], int dim);
void f_partenza(t_corse v[], int dim);
void f_capolinea(t_corse v[], int dim);
void f_ritardo(t_corse v[],int dim);
void f_ritardo_tot(t_corse v[], int dim);
void f_ordina(t_corse v[], int dim);
void ordinaPerTratta(t_corse v[], int dim);
void ordinaPerData(t_corse *v, int dim);
void ordinaPerPartenze(t_corse *v, int dim);
void ordinaPerArrivo(t_corse *v, int dim);
void linearSearch(t_corse v[], int dim, char chiave[MAXS+1]);
void binSearch(t_corse v[], int dim, char chiave[MAXS+1]);

int main(){
    t_corse corse[NMAX],tmp;
    int i,n,continua;
    e_comando cmd;
    FILE *in;
    in = fopen("corse.txt","r");
    fscanf(in, "%d", &n);
    for(i=0; i<n; i++){
        fscanf(in,"%s %s %s %s %s %s %d",corse[i].cod,corse[i].partenza,corse[i].dest,corse[i].data,corse[i].ora_p,corse[i].ora_a,&corse[i].ritardo);
        corse[i].gdata = data_to_int(corse[i].data);
        corse[i].ora = ora_to_int(corse[i].ora_a);
        printf("%d %d\n",corse[i].gdata,corse[i].ora);
    }
    continua = 1;
    while(continua){
        cmd = leggiComando();
        continua = selezionaDati(corse,n,cmd);
    }
    return 0;
}

e_comando leggiComando(){
    e_comando cmd;
    char stringa[13];
    printf("> ");
    scanf("%s", stringa);
    strlower(stringa);
    if(strcmp(stringa,"stampa")==0)
        cmd = r_stampa;
    if(strcmp(stringa,"tratta")==0)
        cmd = cod_tratta;
    if(strcmp(stringa,"date")==0)
        cmd = r_date;
    if(strcmp(stringa,"partenza")==0)
        cmd = r_partenza;
    if(strcmp(stringa,"capolinea")==0)
        cmd = r_capolinea;
    if(strcmp(stringa,"ritardo")==0)
        cmd = r_ritardo;
    if(strcmp(stringa,"ritardo_tot")==0)
        cmd = r_ritardo_tot;
    if(strcmp(stringa,"fine")==0)
        cmd = r_fine;
    if(strcmp(stringa,"ordina") == 0)
        cmd = r_ordina;
    if(strcmp(stringa,"ricerca")==0)
        cmd = r_ricerca;
    return cmd;
}

int selezionaDati(t_corse v[], int dim, e_comando cmd){
    FILE *fp;
    char c,filename[30],tratta[30];
    switch(cmd){
        case r_stampa:
                printf("Vuoi stampare su file?[Y/N]");
                scanf("%*c %c", &c);
                if(c == 'Y' || c == 'y'){
                    printf("Nome file: ");
                    scanf("%s", filename);
                    fp = fopen(filename,"w");
                }
                else
                    fp = stdout;
                f_stampa(fp,v,dim);
            break;
        case r_date:
            f_date(v,dim);
            break;
        case r_partenza:
            f_partenza(v,dim);
            break;
        case r_capolinea:
            f_capolinea(v,dim);
            break;
        case r_ritardo:
            f_ritardo(v,dim);
            break;
        case r_ritardo_tot:
            f_ritardo_tot(v,dim);
            break;
        case r_ordina:
            f_ordina(v,dim);
            break;
        case r_ricerca:
            printf("Inserisci la stazione da cercare:");
            scanf("%s",tratta);
            if(ord == 0)
                linearSearch(v,dim,tratta);
            else
                binSearch(v,dim,tratta);
        case r_fine:
           return 0;

    }
    return 1;
}

void strlower(char stringa[]){
    int i;
    for(i=0; stringa[i] != 0; i++) stringa[i] = tolower(stringa[i]);
    return;
}

void stampa(FILE *in, t_corse t){
    fprintf(in,"%s %s %s %s %s %s %d\n",t.cod,t.partenza,t.dest,t.data,t.ora_p,t.ora_a,t.ritardo);
}

void f_stampa(FILE *in, t_corse *v, int dim){
    int i;
    for(i=0; i<dim; i++)
        stampa(in,v[i]);
    return;
}

int data_to_int(char stringa[11]){
    int a,m,g;
    sscanf(stringa,"%d/%d/%d",&a,&m,&g);
    return 365*a + 30*m + g;
}

int ora_to_int(char stringa[11]){
    int h,m,s;
    sscanf(stringa,"%d:%d:%d",&h,&m,&s);
    return 3600*h+60*m+s;
}

void f_date(t_corse v[],int dim){
    char data1[11],data2[11];
    int d1,d2;
    int i;
    printf("Inserire due date(formato AAAA/MM/GG)> ");
    scanf("%s %s",data1,data2);
    d1 = data_to_int(data1); d2 = data_to_int(data2);
    for(i=0; i<dim; i++){
        if(data_to_int(v[i].data) > d1 && data_to_int(v[i].data) < d2)
            stampa(stdout,v[i]);
    }
    return;
}

void f_partenza(t_corse v[], int dim){
    int i;
    char partenza[MAXS];
    printf("Inserisci una partenza> ");
    scanf("%s",partenza);
    for (i = 0;i<dim;i++)
        if(strcmp(partenza,v[i].partenza)==0)
            stampa(stdout,v[i]);
    return;
}

void f_capolinea(t_corse v[], int dim){
    int i;
    char capolinea[MAXS];
    printf("Inserisci una partenza> ");
    scanf("%s",capolinea);
    for (i = 0;i<dim;i++)
        if(strcmp(capolinea,v[i].dest)==0)
            stampa(stdout,v[i]);
}

void f_ritardo(t_corse v[], int dim){
    char data1[11],data2[11];
    int d1,d2,i;
    printf("Inserire due date> ");
    scanf("%s %s",data1,data2);
    d1 = data_to_int(data1); d2 = data_to_int(data2);
    printf("%d %d",d1,d2);
    for(i=0; i<dim; i++){
        if(data_to_int(v[i].data) > d1 && data_to_int(v[i].data) < d2 && v[i].ritardo >0)
            stampa(stdout,v[i]);
    }
    return;
}

void f_ritardo_tot(t_corse v[], int dim){
    int i,ritardotot=0;
    char cod[MAXS];
    scanf("%s",cod);
    for (i = 0; i <dim; i++) {
        if(strcmp(v[i].cod,cod)==0)
            ritardotot += v[i].ritardo;
    }
    printf("%s %d\n",cod,ritardotot);
}
void ordinaPerTratta(t_corse v[], int dim){
    int i,j;
    t_corse tmp;
    for(i=0;i<dim-1;i++)
        for(j=0;j<dim-i-1;j++)
            if(strcmp(v[j].cod,v[j+1].cod)>0){
                tmp = v[j];
                v[j] = v[j+1];
                v[j+1] = tmp;
            }
    return;
}

void ordinaPerData(t_corse *v, int dim){
    int i,j,flag;
    t_corse tmp;
    for(i=0;i<dim-1 && flag == 1;i++){
        flag = 0;
        for(j=0;j<dim-i-1;j++)
            if(v[j].ora > v[j+1].ora){
                flag = 1;
                tmp = v[j];
                v[j] = v[j+1];
                v[j+1] = tmp;
            }
    }
    for(i=0;i<dim-1 && flag == 1;i++){
        flag = 0;
        for(j=0;j<dim-i;j++)
            if(v[j].gdata > v[j+1].gdata){
                flag = 1;
                tmp = v[j];
                v[j] = v[j+1];
                v[j+1] = tmp;
            }
    }
    return;
}

void ordinaPerPartenze(t_corse *v, int dim){
    int i,j;
    t_corse tmp;
    for(i=0;i<dim-1;i++){
        for(j=0;j<dim-i-1;j++)
            if(strcmp(v[j].partenza,v[j+1].partenza)>0){
                tmp = v[j];
                v[j] = v[j+1];
                v[j+1] = tmp;
            }
    }
    return;
}

void ordinaPerArrivo(t_corse *v, int dim){
    int i,j;
    t_corse tmp;
    for(i=0;i<dim-1;i++){
        for(j=0;j<dim-i-1;j++)
            if(strcmp(v[j].dest,v[j+1].dest)>0){
                tmp = v[j];
                v[j] = v[j+1];
                v[j+1] = tmp;
            }
    }
    return;
}
void f_ordina(t_corse *v, int dim){
    int i,j;
    t_corse tmp;
    e_comando chiave;
    printf("Su quale chiave vuoi ordinare?");
    chiave = leggiComando();
    switch(chiave){
        case r_date:
            printf("ordino per data\n");
            ordinaPerData(v,dim);
            ord = 1;
            break;
        case r_capolinea:
            printf("ordino per destinazione\n");
            ordinaPerArrivo(v,dim);
            ord = 1;
            break;
        case r_partenza:
            printf("ordino per partenza\n");
            ordinaPerPartenze(v,dim);
            ord = 0;
            break;
        case cod_tratta:
            printf("ordino per cod tratta\n");
            ordinaPerTratta(v,dim);
            ord = 1;
            break;
    }
    return;
}

void linearSearch(t_corse v[], int dim, char chiave[MAXS+1]){
    int i;
    for(i=0;i<dim;i++)
        if(strcmp(v[i].partenza,chiave)==0)
            stampa(stdout, v[i]);
        return;
}

void binSearch(t_corse v[], int dim, char chiave[MAXS+1]){
    int l=0,r = dim-1,m,found = 0,i;
    while(l <=r && found == 0){
        m = (l+r)/2;
        if(strcmp(v[m].partenza,chiave)==0)
            found = 1;
        else if(strcmp(v[m].partenza,chiave)<0)
            l = m+1;
        else
            r = m-1;
    }
    if(found == 1){
        for(i=m; strcmp(chiave,v[i].partenza)==0;i++)
            stampa(stdout,v[i]);
        for(i=m-1; strcmp(chiave,v[i].partenza)==0;i--)
            stampa(stdout,v[i]);
    }
    return;
}