#include <stdio.h>
#include <stdlib.h>
#define MAX 255

typedef struct{
    int num_scelte;
    int scelte[5];
}livello;

int stampa_playlist(char **tabella,int pos, livello *val, int *sol, int n, int count);

int main(){
    FILE *in;
    livello *val;
    int *sol;
    int n,i,j,k=0,n_playlist;
    char **tabella;
    in = fopen("brani.txt","r");
    fscanf(in,"%d",&n);
    val = malloc(n*sizeof(livello));
    sol = (int*) malloc(n*sizeof(int));
    tabella = malloc(5*n* sizeof(char *));
    for(i=0;i<n;i++){
        fscanf(in,"%d",&val[i].num_scelte);
        for(j=0;j<val[i].num_scelte;j++){
            val[i].scelte[j] = k;
            tabella[k] =(char*) malloc(MAX*sizeof(char));
            fscanf(in,"%s",tabella[k]);
            k++;
        }
    }
    n_playlist = stampa_playlist(tabella,0,val,sol,n,0);
    printf("trovate %d soluzioni",n_playlist);
    return 0;
}

int stampa_playlist(char **tabella,int pos, livello *val, int *sol, int n, int count){
    int i;
    if(pos >= n){
        for(i=0;i<n;i++)
            printf("%s, ",tabella[sol[i]]);
        printf("\n");
        return count+1;
    }
    for(i=0; i<val[pos].num_scelte;i++){
        sol[pos] = val[pos].scelte[i];
        count =stampa_playlist(tabella,pos+1,val,sol,n,count);
    }
    return count;
}