#include <stdio.h>
#include <stdlib.h>

int **malloc2d(FILE *fp,int nr, int nc);
void separa(int **mat, int nr, int nc, int *b, int *n);

int main(){
    FILE *in;
    int nr,nc,**matrice,*bianchi=NULL,*neri=NULL;
    in = fopen("mat.txt","r");
    fscanf(in, "%d %d",&nr,&nc);
    matrice = malloc2d(in,nr,nc);
    fclose(in);
    separa(matrice,nr,nc,bianchi,neri);
    fclose(in);
    return 0;
}

int **malloc2d(FILE *fp,int nr, int nc){
    int **m,i,j;
    m = malloc(nr * sizeof(int*));
    for(i=0;i<nr;i++){
        m[i] = malloc(nc* sizeof(int));
        for(j=0;j<nc;j++)
            fscanf(fp,"%d",&m[i][j]);
    }
    return m;
}

void separa(int **mat, int nr, int nc, int *b, int *n){
    int i,j,k=0,ib=0,in=0;
    b = malloc((nr*nc)/2* sizeof(int));
    n = malloc((((nr*nc)/2)+1)* sizeof(int));
    for(i=0;i<nr;i++){
        for(j=0;j<nc;j++){
            if(k%2==0)
               n[in++] = mat[i][j];
            else
               b[ib++] = mat[i][j];
            k++;
        }
    }
    printf("NERI\n");
    for(i=0;i<nr*nc/2+1;i++)
        printf("%d ",n[i]);
    printf("\nBIANCHI\n");
    for(i=0;i<nr*nc/2;i++)
        printf("%d ",b[i]);
    free(b);
    free(n);
    return;
}
