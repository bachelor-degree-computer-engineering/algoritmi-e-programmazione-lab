#include <stdio.h>
#include <stdlib.h>

typedef struct{
    int u;
    int v;
}arco;

int check(int *sol, int n, arco *archi, int e);
int vertexCover(int *nodi, arco *archi, int e, int n, int *sol);
int vertexcoverR(int *val, int k, int *sol, int j,arco *archi, int e, int pos, int start);
int linearSearch(int *v, int n, int key);

int main(){
    FILE *in;
    int i,n,e,*nodi,*sol,count=0;
    arco *archi;
    in = fopen("grafo.txt","r");
    fscanf(in,"%d %d",&n,&e);
    nodi = (int *) malloc(n* sizeof(int));
    archi = (arco * ) malloc(e* sizeof(arco));
    sol = (int *) malloc(n* sizeof(int));
    for(i=0;i<n;i++)
        nodi[i]=i;
    for(i=0;i<e;i++){
        fscanf(in,"%d %d",&archi[i].u,&archi[i].v);
    }
    count = vertexCover(nodi,archi,e,n,sol);
    printf("\nTrovati %d vertex cover",count);
    return 0;
}

int linearSearch(int *v, int n, int key){
    int flag=0,i;
    for(i=0;i<n && flag==0;i++)
        if(v[i] == key)
            flag = 1;
    return flag;
}

int check(int *sol, int n, arco *archi, int e){
    int ok = 1,i;
    for(i=0;i<e && ok ==1;i++){
        if(linearSearch(sol,n,archi[i].u) == 0 && linearSearch(sol,n,archi[i].v) == 0)
            ok = 0;
    }
    return ok;
}

int vertexCover(int *nodi,arco *archi, int e, int n, int *sol) {
    int count = 0, i;
    for (i = 1; i <= n; i++)
        count += vertexcoverR(nodi, n, sol, i,archi,e, 0, 0);
    return count;
}

int vertexcoverR(int *val, int k, int *sol, int j,arco *archi, int e, int pos, int start){
    int i,count=0;
    if(pos == j && check(sol,j,archi,e)){
        printf("{ ");
        for(i=0; i<j-1;i++){
            printf("%d, ",sol[i]);
        }
        printf("%d }\n",sol[j-1]);
        return 1;
    }
    for(i=start;i<k;i++){
        sol[pos] = val[i];
        count += vertexcoverR(val,k,sol,j,archi,e,pos+1,i+1);
    }
    return count;
}