#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct{
    char codice[6];
    char nome[51];
    char cognome[51];
    char data[11]; // nel formato gg/mm/aaaa
    int datad;
    char via[51];
    char citta[51];
    int cap;
}Item;

typedef struct node *link;
typedef int key;
typedef enum{ r_stampa, r_ricerca, r_cancella_codice, r_cancella_data, r_fine}cmd;

struct node{
    Item val;
    link next;
};

int dataToInt(char *s);
void Itemprint(FILE *out,Item t);
Item Iteminput(FILE *in);
Item itemSetVoid();
key getKey(Item t);
link newNode(Item val, link next);
link inserimentoOrdinato(link h, Item val);
void stampa(link h,FILE *out);
link leggiFile(FILE *in);
Item ricercaCodice(link h, char* k);
Item estraicodice(link *hp, char *k);
Item estraidate(link *hp, int data1, int data2);
cmd seleziona_comando(char *s);

int main(){
    FILE *in,*out;
    link head = NULL;
    Item t;
    char s[30],s2[30], cod[6];
    int continua = 1,d1,d2;
    cmd comando;
    in = fopen("lista.txt","r");
    head = leggiFile(in);
    fclose(in);
    t = ricercaCodice(head, "A1241");
    Itemprint(stdout, t);
    printf("Caricata lista da file lista.txt\n\n");
    stampa(head, stdout);
    while(continua == 1){
        printf(">");
        scanf("%s",s);
        comando = seleziona_comando(s);
        switch(comando){
            case r_stampa:
                out = fopen("log.txt","w");
                stampa(head,out);
                fclose(out);
                break;
            case r_ricerca:
                printf("Inserisci il codice: ");
                scanf("%s",cod);
                t = ricercaCodice(head, cod);
                if(strcmp(t.codice,"00000") == 0) printf("Non trovato");
                else {
                    printf("Trovata: ");
                    Itemprint(stdout,t);
                }
                break;
            case r_cancella_codice:
                printf("Inserisci il codice: ");
                scanf("%s ",s);
                t = estraicodice(&head, s);
                if(strcmp(t.codice,"00000") == 0) printf("Non cancellato perche' elemento non esiste");
                else{
                    printf("Cancellato nodo: ");
                    Itemprint(stdout,t);
                }
                break;
            case r_cancella_data:
                printf("Inserisci le due date: ");
                scanf("%s %s",s,s2);
                d1 = dataToInt(s); d2 = dataToInt(s2);
                do{
                    t = estraidate(&head,d1,d2);
                    printf("Cancellato: ");
                    Itemprint(stdout,t);
                }while(strcmp(t.codice,"00000") != 0);
                break;
            case r_fine:
                continua = 0;
                break;
        }
    }
    return 0;
}

cmd seleziona_comando(char *s){
    if(strcmp(s,"stampa") == 0)
        return r_stampa;
    if(strcmp(s,"ricerca") == 0)
        return r_ricerca;
    if(strcmp(s,"cancella_cod") == 0)
        return r_cancella_codice;
    if(strcmp(s,"cancella_date") == 0)
        return r_cancella_data;
    if(strcmp(s,"fine") == 0)
        return r_fine;
    return r_fine;
}

int dataToInt(char *s){
    int a,m,g;
    sscanf(s,"%d/%d/%d",&g,&m,&a);
    return 500*a+31*m+g;
}

void Itemprint(FILE *out,Item t){
    fprintf(out,"%s %s %s %s %s %s %d\n",t.codice,t.nome,t.cognome,t.data,t.via,t.citta,t.cap);
    return;
}

Item itemSetVoid(){
    Item t;
    strcpy(t.codice,"00000");
    return t;
}

key getKey(Item t){
    return t.datad;
}

int keyless(key a, key b){
    return a < b;
}


link newNode(Item val, link next){
    link x = malloc(sizeof *x);
    if(x == NULL)
        return NULL;
    x->val = val;
    x->next = next;
    return x;
}

link inserimentoOrdinato(link h, Item val){
    link x,p;
    key k = getKey(val);
    if(h == NULL || keyless(getKey(h->val),k))
        return newNode(val,h);
    for(x=h->next,p=h; x!=NULL && keyless(k,getKey(x->val)); p=x, x=x->next);
    p->next = newNode(val,x);
    return h;
}

void stampa(link h, FILE *out){
    link x;
    for(x=h;x!=NULL;x=x->next)
        Itemprint(out,x->val);
}

link leggiFile(FILE *in){
    link h=NULL;
    Item t;
    while(fscanf(in, "%s %s %s %s %s %s %d",t.codice,t.nome,t.cognome,t.data,t.via,t.citta,&t.cap) != -1){
        t.datad = dataToInt(t.data);
        h = inserimentoOrdinato(h,t);
    }
    return h;
}

Item ricercaCodice(link h, char* k){
    link x;
    for(x=h; x!=NULL; x = x->next){
        if(strcmp(k,(x->val).codice) == 0)
            return x->val;
    }
    return itemSetVoid();
}

Item estraicodice(link *hp, char *k){
    link *xp, t;
    Item i = itemSetVoid();
    for(xp=hp; (*xp)!=NULL; xp =&((*xp)->next)){
        if(strcmp(k, ((*xp)->val).codice)==0){
            t = *xp;
             *xp = (*xp)->next;
             i = t->val;
             free(t);
        }
    }
    return i;
}

Item estraidate(link *hp, int data1, int data2){
    link *xp, t;
    Item i = itemSetVoid();
    for(xp=hp; (*xp)!=NULL && getKey((*xp)->val) < data2 ; xp = &((*xp)->next)){
        if(getKey((*xp)->val) > data1 && getKey((*xp)->val) < data2){
            t = *xp;
            *xp = (*xp)->next;
            i = t ->val;
            free(t);
        }
    }
    return i;
}