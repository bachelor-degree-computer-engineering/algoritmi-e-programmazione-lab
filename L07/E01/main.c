#include <stdio.h>
#include <stdlib.h>

void generacollane(int pietre[4],char nomi[4]);
int generacollaneR(int pos, int *sol, int pietre[4], int l);

int main(){
    int pietre[4]={1,1,1,1};
    int i;
    char nomi[4]={'z','r','t','s'};
    printf("Inserisci le pietre per generare la collana:\n");
    for(i=0;i<4;i++) {
      printf("%c:",nomi[i]);
      scanf("%d", &pietre[i]);
    }
    generacollane(pietre,nomi);
    return 0;
}

void generacollane(int pietre[4], char nomi[4]){
    int lmax=0,i=0,*sol,bestlun=0;

    for(i=0;i<4;i++)
        lmax += pietre[i];
    sol = malloc(lmax* sizeof(int));
    for(i=lmax; i>=1; i--){
        if(generacollaneR(0,sol,pietre,i)) {
            bestlun = i;
            break;
        }
    }
    printf("Soluzione a lunghezza massima: %d\n\n", bestlun);
    for(i=0;i<bestlun;i++)
        printf("%c", nomi[sol[i]]);
    free(sol);
}

int generacollaneR(int pos, int *sol, int *pietre, int l){
    int i;
    if(pos >= l)
        return 1;
    for(i=0;i<4;i++){
        if(pietre[i]>0){
            if (sol != 0) {
                if (((sol[pos-1]==0) || (sol[pos-1]==2)) && (i!=0) && (i!=1))
                    continue;
                if (((sol[pos-1]==3) || (sol[pos-1]==1)) && (i!=2) && (i!=3))
                    continue;
            }
            pietre[i]--;
            sol[pos] = i;
            if(generacollaneR(pos+1,sol,pietre,l))
                return 1;
            pietre[i]++;
        }
    }
    return 0;
}