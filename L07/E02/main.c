#include <stdio.h>
#include <stdlib.h>
#define DBG
void generacollane(int pietre[4],int valpietre[4],char nomi[4], int maxrip);
void generacollaneR(int pos, int *sol, int *pietre, int l,int *valpietre, int *bestsol, int *bestval, int *bestsollen, int maxrip);
int checkopt(int *sol, int l, int *valpietre);

int main(){
    FILE *in;
    int pietre[4], valpietre[4],i,j,numtestset,maxrip;
    char nomi[4]={'z','r','t','s'};
    in = fopen("testset.txt","r");

    fscanf(in,"%d", &numtestset);
    for(i=0; i<numtestset; i++){
        for(j=0;j<4;j++) fscanf(in, "%d",&pietre[j]);
        for(j=0;j<4;j++) fscanf(in, "%d",&valpietre[j]);
        fscanf(in,"%d", &maxrip);
        printf("TEST #%d\n",i+1);
        generacollane(pietre,valpietre,nomi,maxrip);
    }
    return 0;
}

void generacollane(int pietre[4],int valpietre[4], char nomi[4], int maxrip){
    int lmax=0,i=0,*sol,*bestsol,bestlen=0,bestval=0;
    for(i=0;i<4;i++)
        lmax += pietre[i];
    printf("zaffiro = %d [%d], rubino = %d [%d], topazio = %d [%d], smeraldo = %d [%d], TOT = %d {max_rip = %d}\n",pietre[0],valpietre[0],pietre[1],valpietre[1],pietre[2],valpietre[2],pietre[3],valpietre[3],lmax,maxrip);
#ifdef DBG
    if(lmax > 29) return;
#endif
    sol = malloc(lmax* sizeof(int));
    bestsol = malloc(lmax* sizeof(int));
    for(i=lmax; i>0; i--){
        generacollaneR(0,sol,pietre,i,valpietre,bestsol,&bestval,&bestlen,maxrip);
    }
    printf("Soluzione ottima di valore %d usando %d gemme.\nComposizione collana: ",bestval,bestlen);
    for(i=0;i<bestlen;i++)
        printf("%c", nomi[sol[i]]);
    printf("\n");
    free(sol);
    free(bestsol);
}

int checkopt(int *sol, int l, int *valpietre){
    int *occ,val=0,i;
    occ = calloc(4, sizeof(int));
    for(i=0;i<l;i++)
        occ[sol[i]]++;
    for(i=0;i<4;i++)
        val += occ[i]*valpietre[i];
    if(occ[3] < occ[0]) val = -1;
    return val;

}
void generacollaneR(int pos, int *sol, int *pietre, int l,int *valpietre, int *bestsol, int *bestval, int *bestsollen, int maxrip){
    int i,j,count,tmpval;
    if(pos >= l){
        tmpval = checkopt(sol,l,valpietre);
        if(tmpval > *bestval && l > *bestsollen){
            *bestsollen = l;
            *bestval = tmpval;
            for(j=0;j<l;j++)
                bestsol[j] = sol[j];
        }
        return;
    }
    for(i=0;i<4;i++){
        if(pietre[i]>0){
            if (sol != 0) {
                if (((sol[pos-1]==0) || (sol[pos-1]==2)) && (i!=0) && (i!=1))
                    continue;
                if (((sol[pos-1]==3) || (sol[pos-1]==1)) && (i!=2) && (i!=3))
                    continue;
                for(j = pos-1, count = 1; j >= 0 && sol[j] == i && count < maxrip+1; j--, count++);
                if(count > maxrip)
                    continue;
                if(checkopt(sol,pos,valpietre) == -1)
                    continue;
            }
            pietre[i]--;
            sol[pos] = i;
            generacollaneR(pos+1, sol, pietre,l,valpietre,bestsol,bestval,bestsollen,maxrip);
            pietre[i]++;
        }
    }
    return;
}