#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef enum{
    aggiungi, rimuovi
}sel;

typedef enum{
    continua, r_aggiungi, r_rimuovi, r_equipaggia, r_stampa, r_disequipaggia, r_fine
}cmd;

typedef struct{
    int hp;
    int mp;
    int atk;
    int def;
    int mag;
    int spr;
}stat_t;

typedef struct{
    char *nome;
    char *tipo;
    stat_t mod;
}inv_t;

typedef struct{
    inv_t *vettinv;
    int nInv;
    int maxInv;
}tabInv;

typedef struct{
    int inUso;
    inv_t **equip;
}tabEquip_t;

typedef struct{
    char codice[7];
    char *nome;
    char *classe;
    tabEquip_t *equip;
    stat_t statbase;
    stat_t cstat;
}pg_t;

typedef struct node *link, nodePG;

struct node{
    pg_t val;
    link next;
};

typedef struct{
    link headPG;
    link tailPG;
    int nPG;
}tabPG;

link creaPG(pg_t val, link next);
int TailIns(tabPG* tab,pg_t val);
tabPG *creatabPG();
tabPG *caricatabPG(FILE *in);
tabPG *aggiungiPG(tabPG *tab);
tabPG *eliminaPG(tabPG *tab);
void pgprint(FILE *out, pg_t tmp);
void stampatabPG(FILE *out, tabPG *tabp);
tabInv *creaInventario(FILE *in);
void stampaInv(FILE *out, tabInv *tabInv1);
tabEquip_t *creaEquip();
link ricercaPG(char cod[7],tabPG *tabellapg);
void aggiungiEquip(link pg, tabInv *tabellainv);
void rimuoviEquip(link pg, tabInv *tabellainv);
stat_t aggiornaStat(link pg, int pos, sel tipo);
cmd selezionaDati(char *str);

int main(){
    tabPG *tabellapg;
    tabInv *tabellainv;
    FILE *in;
    char cod[7],strcom[25]; link pg;
    cmd comando = continua;

    in = fopen("personaggi.txt", "r");
    tabellapg = caricatabPG(in);
    fclose(in);
    stampatabPG(stdout,tabellapg);

    in = fopen("inventario.txt","r");
    tabellainv = creaInventario(in);
    stampaInv(stdout, tabellainv);
    fclose(in);

    while(comando != r_fine){
        printf(">");
        scanf("%s",strcom);
        comando = selezionaDati(strcom);
        switch(comando){
            case r_aggiungi:
                tabellapg = aggiungiPG(tabellapg);
                stampatabPG(stdout,tabellapg);
                break;
            case r_rimuovi:
                tabellapg = eliminaPG(tabellapg);
                stampatabPG(stdout,tabellapg);
                break;
            case r_equipaggia:
                printf("Inserisci il codice pg da equipaggiare:");
                scanf("%s",cod);
                pg = ricercaPG(cod,tabellapg);
                if(pg != NULL)
                    aggiungiEquip(pg,tabellainv);
                break;
            case r_disequipaggia:
                printf("Inserisci il codice pg da cui rimuovere equipaggiamento:");
                scanf("%s",cod);
                pg = ricercaPG(cod,tabellapg);
                if(pg != NULL)
                    rimuoviEquip(pg,tabellainv);
                break;
            case r_stampa:
                stampatabPG(stdout,tabellapg);
                break;
            default:
                break;
        };
    }
    return 0;
}
cmd selezionaDati(char *str){
    if(strcmp(str,"aggiungipg")==0)
        return r_aggiungi;
    if(strcmp(str,"rimuovipg") == 0)
        return r_rimuovi;
    if(strcmp(str, "aggiungiequip")==0)
        return r_equipaggia;
    if(strcmp(str,"rimouviequip")==0)
        return r_disequipaggia;
    if(strcmp(str,"stampa")==0)
        return r_stampa;
    return r_fine;

}
tabEquip_t *creaEquip(){
    tabEquip_t *t;
    int i;
    t = (tabEquip_t *) malloc(sizeof(tabEquip_t));
    t->inUso = 0;
    t->equip = malloc(8* sizeof(inv_t *));
    for(i=0; i<8; i++)
        t->equip[i] = NULL;
    return t;
}

tabPG *creatabPG(){
    tabPG *p;
    p =(tabPG*) malloc(sizeof(tabPG));
    p->headPG = NULL;
    p->tailPG = NULL;
    p->nPG = 0;
    return p;
}

link creaPG(pg_t val, link next){
    link t;
    t = malloc(sizeof(nodePG));
    t->val = val;
    t->next = next;
    return t;
}

int TailIns(tabPG *tab, pg_t val){
    if(tab->headPG == NULL){
        tab->headPG = creaPG(val, NULL);
        tab->tailPG = tab->headPG;
        tab->nPG++;
    }
    else{
        tab->tailPG->next = creaPG(val,NULL);
        tab->tailPG = tab->tailPG->next;
        tab->nPG++;
    }
    return 0;
}

tabPG *aggiungiPG(tabPG *tab){
    tabPG *t = tab;
    pg_t tmp;
    char nome[51], classe[51];
    printf("Inserisci da tastiera i dati del nuovo personaggio:\n>");
    fscanf(stdin, "%s %s %s %d %d %d %d %d %d", tmp.codice, nome, classe, &(tmp.statbase.hp),
           &(tmp.statbase.mp), &(tmp.statbase.atk), &(tmp.statbase.def), &(tmp.statbase.mag),
           &(tmp.statbase.spr));
    tmp.cstat = tmp.statbase;
    tmp.nome = strdup(nome);
    tmp.classe = strdup(classe);
    TailIns(t,tmp);
    return t;
}

tabPG *eliminaPG(tabPG *tab){
    link x,p;
    tabPG *tmp = tab;
    char cod[7];
    printf("Inserisci il codice del personaggio da eliminare: ");
    scanf("%s",cod);
    for(p=NULL,x=tmp->headPG; x!=NULL;p=x, x=x->next){
        if(strcmp(cod,x->val.codice) == 0){
            if(x == tmp->headPG)
                tmp->headPG = x->next;
            else
                p->next = x->next;
            free(x);
            break;
        }
    }
    printf("Cancellato personaggio con codice %s\n",cod);
    return tmp;
}
tabPG *caricatabPG(FILE *in){
    tabPG *t;
    pg_t tmp;
    int v;
    char nome[51], classe[51];
    t = creatabPG();
    while(fscanf(in, "%s %s %s %d %d %d %d %d %d", tmp.codice, nome, classe, &(tmp.statbase.hp),
                   &(tmp.statbase.mp), &(tmp.statbase.atk), &(tmp.statbase.def), &(tmp.statbase.mag),
                   &(tmp.statbase.spr))!=-1){
        tmp.nome = strdup(nome);
        tmp.classe =strdup(classe);
        tmp.equip = creaEquip();
        tmp.cstat = tmp.statbase;
        TailIns(t, tmp);
    }
    return t;
}

void pgprint(FILE *out,pg_t tmp){
    fprintf(out,"%s %s %s %d %d %d %d %d %d\n",tmp.codice,tmp.nome,tmp.classe,tmp.cstat.hp,tmp.cstat.mp,tmp.cstat.atk,tmp.cstat.def,tmp.cstat.mag,tmp.cstat.spr);
}

void stampatabPG(FILE *out, tabPG *tabp){
    link x;
    for(x = tabp->headPG; x!= NULL; x=x->next)
        pgprint(out,x->val);
}

tabInv *creaInventario(FILE *in){
    tabInv *t;
    int i;
    inv_t tmp;
    char nome[51],tipo[51];
    t =(tabInv*)  malloc(sizeof(tabInv));
    t->maxInv = 0;
    fscanf(in,"%d",&(t->nInv));
    t->vettinv = (inv_t *) malloc((t->nInv)* sizeof(inv_t));
    for(i=0; i<t->nInv; i++){
        fscanf(in,"%s %s %d %d %d %d %d %d",nome,tipo,&(t->vettinv[i].mod.hp),&(t->vettinv[i].mod.mp),&(t->vettinv[i].mod.atk),&(t->vettinv[i].mod.def),&(t->vettinv[i].mod.mag),&(t->vettinv[i].mod.spr));
        t->vettinv[i].nome = strdup(nome);
        t->vettinv[i].tipo = strdup(tipo);
    }
    return t;
}

void stampaInv(FILE *out, tabInv *t){
    int i;
    printf("\n\n\n");
    for(i=0; i<t->nInv; i++){
        fprintf(stdout,"%s %s %d %d %d %d %d %d\n",t->vettinv[i].nome,t->vettinv[i].tipo,t->vettinv[i].mod.hp,t->vettinv[i].mod.mp,t->vettinv[i].mod.atk,t->vettinv[i].mod.def,t->vettinv[i].mod.mag,t->vettinv[i].mod.spr);
    }
}

link ricercaPG(char cod[7], tabPG *tabellapg){
    link pgpointer = NULL,x;
    for(x=tabellapg->headPG; x!=NULL; x=x->next){
        if(strcmp(x->val.codice,cod) == 0)
            return x;
    }
    return pgpointer;
}

void aggiungiEquip(link pg, tabInv *tabellainv){
    int pos=-1,i, equipos;
    char nome[51];
    printf("Inserisci il nome dell'oggetto da equipaggiare a:\n");
    pgprint(stdout, pg->val);
    scanf("%s", nome);
    for(i=0; i<tabellainv->nInv; i++){
        if(strcmp(nome,tabellainv->vettinv[i].nome)==0)
            pos = i;
    }
    if(pos!=-1){
        equipos = pg->val.equip->inUso;
        pg->val.equip->equip[equipos] = &(tabellainv->vettinv[pos]);
        pg->val.equip->inUso++;
        pg->val.cstat = aggiornaStat(pg, equipos, aggiungi);
    }
    pgprint(stdout,pg->val);
}

void rimuoviEquip(link pg, tabInv *tabellainv){
    int i;
    char nome[51];
    printf("Inserisci il nome dell'oggetto da rimuovere all'equipaggiamento a: \n ");
    pgprint(stdout,pg->val);
    scanf("%s",nome);
    for(i=0; i<8; i++){
        if(strcmp(nome,pg->val.equip->equip[i]->nome) == 0) {
            pg->val.cstat = aggiornaStat(pg, i, rimuovi);
            pg->val.equip->equip[i] = NULL;
            return;
        }
    }
    printf("Non è stato trovato l'oggetto equipaggiato");
}

stat_t aggiornaStat(link pg, int pos, sel tipo){
    stat_t t = pg->val.cstat;
    if(tipo == aggiungi) {
        t.hp += pg->val.equip->equip[pos]->mod.hp;   if(t.hp < 0)   t.hp = 1;
        t.mp += pg->val.equip->equip[pos]->mod.mp;   if(t.mp < 0)   t.mp = 1;
        t.atk += pg->val.equip->equip[pos]->mod.atk; if(t.atk < 0) t.atk = 1;
        t.def += pg->val.equip->equip[pos]->mod.def; if(t.def < 0) t.def = 1;
        t.mag += pg->val.equip->equip[pos]->mod.mag; if(t.mag < 0) t.mag = 1;
        t.spr += pg->val.equip->equip[pos]->mod.spr; if(t.spr < 0) t.spr = 1;
    } else if(tipo == rimuovi){
        t.hp -= pg->val.equip->equip[pos]->mod.hp;   if(t.hp < 0)   t.hp = 1;
        t.mp -= pg->val.equip->equip[pos]->mod.mp;   if(t.mp < 0)   t.mp = 1;
        t.atk -= pg->val.equip->equip[pos]->mod.atk; if(t.atk < 0) t.atk = 1;
        t.def -= pg->val.equip->equip[pos]->mod.def; if(t.def < 0) t.def = 1;
        t.mag -= pg->val.equip->equip[pos]->mod.mag; if(t.mag < 0) t.mag = 1;
        t.spr -= pg->val.equip->equip[pos]->mod.spr; if(t.spr < 0) t.spr = 1;
    }
    return t;
}