#include <stdio.h>
#include <stdlib.h>

typedef struct{
    int s,f;
}att;

int compatibili(att i, att j);
int checkcomp(int *sol,att* v,int pos);
void attsel(int N, att *v);
void attselR(int pos, att *val, int N, int *sol, int *bestd, int *bestsol, int *durate, int Cdurata);

int main(){
    FILE *in;
    int N,i;
    att *val;
    in  = fopen("att.txt","r");
    fscanf(in,"%d", &N);
    val = malloc(N* sizeof(att));
    for(i=0; i<N; i++)
        fscanf(in,"%d %d",&val[i].s,&val[i].f);
    attsel(N,val);
    return 0;
}

void attsel(int N, att *v){
    int i,*d, bestd=0;
    int *sol, *bestsol;
    sol = malloc(N* sizeof(int));
    bestsol = malloc(N* sizeof(int));
    d = malloc(N* sizeof(int));
    for(i=0;i<N;i++){
        d[i] = v[i].f-v[i].s;
    }
    attselR(0,v,N,sol,&bestd, bestsol, d, 0);
    printf("Durata massima: %d\n",bestd);
    for(i=0; i<N; i++){
        if(bestsol[i] == 1)
            printf("(%d %d) ",v[i].s,v[i].f);
    }
    free(sol);
    free(d);
}

void attselR(int pos, att *val, int N, int *sol, int *bestd, int *bestsol, int *durate, int Cdurata){
    int i;
    if(pos == N){
        if(Cdurata > *bestd){
            *bestd = Cdurata;
            for(i=0;i<N; i++)
                bestsol[i] = sol[i];
        }
        return;
    }
    sol[pos] = 0;
    attselR(pos+1,val,N,sol,bestd,bestsol,durate,Cdurata);
    if(checkcomp(sol,val,pos)){
        sol[pos] = 1;
        attselR(pos+1,val,N, sol,bestd,bestsol,durate,Cdurata+durate[pos]);
    }
}

int compatibili(att i, att j){
    if(i.s < j.f && j.s < i.f) return 0;
    return 1;
}

int checkcomp(int *sol, att *v, int pos){
    int i;
    for(i=0; i<pos; i++)
        if(sol[i] == 1 && !compatibili(v[pos],v[i]))
            return 0;
    return 1;
}