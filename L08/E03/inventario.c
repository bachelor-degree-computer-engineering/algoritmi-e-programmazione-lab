#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "inventario.h"

tabInv *creaInventario(FILE *in){
    tabInv *t;
    int i;
    inv_t tmp;
    char nome[51],tipo[51];
    t =(tabInv*)  malloc(sizeof(tabInv));
    t->maxInv = 0;
    fscanf(in,"%d",&(t->nInv));
    t->vettinv = (inv_t *) malloc((t->nInv)* sizeof(inv_t));
    for(i=0; i<t->nInv; i++){
        fscanf(in,"%s %s %d %d %d %d %d %d",nome,tipo,&(t->vettinv[i].mod.hp),&(t->vettinv[i].mod.mp),&(t->vettinv[i].mod.atk),&(t->vettinv[i].mod.def),&(t->vettinv[i].mod.mag),&(t->vettinv[i].mod.spr));
        t->vettinv[i].nome = strdup(nome);
        t->vettinv[i].tipo = strdup(tipo);
    }
    return t;
}

void distruggiInventario(tabInv *inv){
    int i;
    for(i=0;i<inv->nInv;i++){
        free(inv->vettinv[i].nome);
        free(inv->vettinv[i].tipo);
    }
    free(inv->vettinv);
    free(inv);
}

void stampaOggetto(inv_t t){
    fprintf(stdout,"%s %s %d %d %d %d %d %d\n",t.nome,t.tipo,t.mod.hp,t.mod.mp,t.mod.atk,t.mod.def,t.mod.mag,t.mod.spr);
}

void stampaInv(FILE *out, tabInv *t){
    int i;
    printf("\n\n\n");
    for(i=0; i<t->nInv; i++){
        stampaOggetto(t->vettinv[i]);
    }
}

int ricercaPerNome(tabInv *tab, char *s){
    int pos;
    for(pos=0;pos<tab->nInv;pos++){
        if(strcmp(s,tab->vettinv[pos].nome)==0)
            return pos;
    }
    return -1;
}
