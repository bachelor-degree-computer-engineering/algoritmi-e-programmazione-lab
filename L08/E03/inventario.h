#ifndef APALAB_INVENTARIO_H
#define APALAB_INVENTARIO_H

typedef struct{
    int hp;
    int mp;
    int atk;
    int def;
    int mag;
    int spr;
}stat_t;

typedef struct{
    char *nome;
    char *tipo;
    stat_t mod;
}inv_t;

typedef struct{
    inv_t *vettinv;
    int nInv;
    int maxInv;
}tabInv;

tabInv *creaInventario(FILE *in);
void distruggiInventario(tabInv *inv);
void stampaOggetto(inv_t t);
void stampaInv(FILE *out, tabInv *tabInv1);
int ricercaPerNome(tabInv *tab, char *s);
#endif //APALAB_INVENTARIO_H
