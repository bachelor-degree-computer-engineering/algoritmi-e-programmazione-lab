#include <stdio.h>
#include <string.h>
#include "pg.h"
#include "inventario.h"

typedef enum{
    continua, r_aggiungi, r_rimuovi, r_equipaggia, r_stampa, r_disequipaggia, r_fine
}cmd;

cmd selezionaDati(char *str);

int main(){
    tabPG *tabellapg;
    tabInv *tabellainv;
    FILE *in;
    char cod[7],strcom[25]; link pg;
    cmd comando = continua;

    in = fopen("personaggi.txt", "r");
    tabellapg = caricatabPG(in);
    fclose(in);
    stampatabPG(stdout,tabellapg);

    in = fopen("inventario.txt","r");
    tabellainv = creaInventario(in);
    stampaInv(stdout, tabellainv);
    fclose(in);

    while(comando != r_fine){
        printf(">");
        scanf("%s",strcom);
        comando = selezionaDati(strcom);
        switch(comando){
            case r_aggiungi:
                tabellapg = aggiungiPG(tabellapg);
                stampatabPG(stdout,tabellapg);
                break;
            case r_rimuovi:
                tabellapg = eliminaPG(tabellapg);
                stampatabPG(stdout,tabellapg);
                break;
            case r_equipaggia:
                printf("Inserisci il codice pg da equipaggiare:");
                scanf("%s",cod);
                pg = ricercaPG(cod,tabellapg);
                if(pg != NULL)
                    aggiungiEquip(pg,tabellainv);
                break;
            case r_disequipaggia:
                printf("Inserisci il codice pg da cui rimuovere equipaggiamento:");
                scanf("%s",cod);
                pg = ricercaPG(cod,tabellapg);
                if(pg != NULL)
                    rimuoviEquip(pg,tabellainv);
                break;
            case r_stampa:
                stampatabPG(stdout,tabellapg);
                break;
            default:
                break;
        };
    }
    return 0;
}
cmd selezionaDati(char *str){
    if(strcmp(str,"aggiungipg")==0)
        return r_aggiungi;
    if(strcmp(str,"rimuovipg") == 0)
        return r_rimuovi;
    if(strcmp(str, "aggiungiequip")==0)
        return r_equipaggia;
    if(strcmp(str,"rimouviequip")==0)
        return r_disequipaggia;
    if(strcmp(str,"stampa")==0)
        return r_stampa;
    return r_fine;

}
