#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "pg.h"
#include "inventario.h"

tabEquip_t *creaEquip(){
    tabEquip_t *t;
    int i;
    t = (tabEquip_t *) malloc(sizeof(tabEquip_t));
    t->inUso = 0;
    t->equip = malloc(8* sizeof(inv_t *));
    for(i=0; i<8; i++)
        t->equip[i] = NULL;
    return t;
}

void distruggiEquip(tabEquip_t *equip){
    free(equip->equip);
    free(equip);
}

tabPG *creatabPG(){
    tabPG *p;
    p =(tabPG*) malloc(sizeof(tabPG));
    p->headPG = NULL;
    p->tailPG = NULL;
    p->nPG = 0;
    return p;
}

void distruggitabPG(tabPG *tab){
    link x,t;
    for(x=tab->headPG; x!=NULL; x=t){
        t = x->next;
        free(x->val.nome);
        free(x->val.classe);
        distruggiEquip(x->val.equip);
        free(x);
    }
    free(tab);
}

link creaPG(pg_t val, link next){
    link t;
    t = malloc(sizeof(nodePG));
    t->val = val;
    t->next = next;
    return t;
}

int TailIns(tabPG *tab, pg_t val){
    if(tab->headPG == NULL){
        tab->headPG = creaPG(val, NULL);
        tab->tailPG = tab->headPG;
        tab->nPG++;
    }
    else{
        tab->tailPG->next = creaPG(val,NULL);
        tab->tailPG = tab->tailPG->next;
        tab->nPG++;
    }
    return 0;
}

tabPG *aggiungiPG(tabPG *tab){
    tabPG *t = tab;
    pg_t tmp;
    char nome[51], classe[51];
    printf("Inserisci da tastiera i dati del nuovo personaggio:\n>");
    fscanf(stdin, "%s %s %s %d %d %d %d %d %d", tmp.codice, nome, classe, &(tmp.statbase.hp),
           &(tmp.statbase.mp), &(tmp.statbase.atk), &(tmp.statbase.def), &(tmp.statbase.mag),
           &(tmp.statbase.spr));
    tmp.cstat = tmp.statbase;
    tmp.nome = strdup(nome);
    tmp.classe = strdup(classe);
    TailIns(t,tmp);
    return t;
}

tabPG *eliminaPG(tabPG *tab){
    link x,p;
    tabPG *tmp = tab;
    char cod[7];
    printf("Inserisci il codice del personaggio da eliminare: ");
    scanf("%s",cod);
    for(p=NULL,x=tmp->headPG; x!=NULL;p=x, x=x->next){
        if(strcmp(cod,x->val.codice) == 0){
            if(x == tmp->headPG)
                tmp->headPG = x->next;
            else
                p->next = x->next;
            free(x);
            break;
        }
    }
    printf("Cancellato personaggio con codice %s\n",cod);
    return tmp;
}
tabPG *caricatabPG(FILE *in){
    tabPG *t;
    pg_t tmp;
    int v;
    char nome[51], classe[51];
    t = creatabPG();
    while(fscanf(in, "%s %s %s %d %d %d %d %d %d", tmp.codice, nome, classe, &(tmp.statbase.hp),
                 &(tmp.statbase.mp), &(tmp.statbase.atk), &(tmp.statbase.def), &(tmp.statbase.mag),
                 &(tmp.statbase.spr))!=-1){
        tmp.nome = strdup(nome);
        tmp.classe =strdup(classe);
        tmp.equip = creaEquip();
        tmp.cstat = tmp.statbase;
        TailIns(t, tmp);
    }
    return t;
}

void pgprint(FILE *out,pg_t tmp){
    int i;
    inv_t *t;
    fprintf(out,"%s %s %s %d %d %d %d %d %d\n",tmp.codice,tmp.nome,tmp.classe,tmp.cstat.hp,tmp.cstat.mp,tmp.cstat.atk,tmp.cstat.def,tmp.cstat.mag,tmp.cstat.spr);
    fprintf(out,"Equipaggiamento:\n");
    for(i=0;i < 8; i++){
        if(tmp.equip->equip[i] != NULL){
            t = tmp.equip->equip[i];
            fprintf(stdout,"%s %s %d %d %d %d %d %d\n",t->nome,t->tipo,t->mod.hp,t->mod.mp,t->mod.atk,t->mod.def,t->mod.mag,t->mod.spr);
        }

    }
}

void stampatabPG(FILE *out, tabPG *tabp){
    link x;
    for(x = tabp->headPG; x!= NULL; x=x->next)
        pgprint(out,x->val);
}


link ricercaPG(char cod[7], tabPG *tabellapg){
    link pgpointer = NULL,x;
    for(x=tabellapg->headPG; x!=NULL; x=x->next){
        if(strcmp(x->val.codice,cod) == 0)
            return x;
    }
    return pgpointer;
}

void aggiungiEquip(link pg, tabInv *tabellainv){
    int pos, equipos;
    char nome[51];
    printf("Inserisci il nome dell'oggetto da equipaggiare a:\n");
    pgprint(stdout, pg->val);
    scanf("%s", nome);
    pos = ricercaPerNome(tabellainv,nome);
    if(pos!=-1){
        equipos = pg->val.equip->inUso;
        pg->val.equip->equip[equipos] = &(tabellainv->vettinv[pos]);
        pg->val.equip->inUso++;
        pg->val.cstat = aggiornaStat(pg, equipos, aggiungi);
    }
    pgprint(stdout,pg->val);
}

void rimuoviEquip(link pg, tabInv *tabellainv){
    int i;
    char nome[51];
    printf("Inserisci il nome dell'oggetto da rimuovere all'equipaggiamento a: \n ");
    pgprint(stdout,pg->val);
    scanf("%s",nome);
    for(i=0; i<8; i++){
        if(strcmp(nome,pg->val.equip->equip[i]->nome) == 0) {
            pg->val.cstat = aggiornaStat(pg, i, rimuovi);
            pg->val.equip->equip[i] = NULL;
            return;
        }
    }
    printf("Non è stato trovato l'oggetto equipaggiato");
}

stat_t aggiornaStat(link pg, int pos, sel tipo) {
    stat_t t = pg->val.cstat;
    if (tipo == aggiungi) {
        t.hp += pg->val.equip->equip[pos]->mod.hp;
        if (t.hp < 0) t.hp = 1;
        t.mp += pg->val.equip->equip[pos]->mod.mp;
        if (t.mp < 0) t.mp = 1;
        t.atk += pg->val.equip->equip[pos]->mod.atk;
        if (t.atk < 0) t.atk = 1;
        t.def += pg->val.equip->equip[pos]->mod.def;
        if (t.def < 0) t.def = 1;
        t.mag += pg->val.equip->equip[pos]->mod.mag;
        if (t.mag < 0) t.mag = 1;
        t.spr += pg->val.equip->equip[pos]->mod.spr;
        if (t.spr < 0) t.spr = 1;
    } else if (tipo == rimuovi) {
        t.hp -= pg->val.equip->equip[pos]->mod.hp;
        if (t.hp < 0) t.hp = 1;
        t.mp -= pg->val.equip->equip[pos]->mod.mp;
        if (t.mp < 0) t.mp = 1;
        t.atk -= pg->val.equip->equip[pos]->mod.atk;
        if (t.atk < 0) t.atk = 1;
        t.def -= pg->val.equip->equip[pos]->mod.def;
        if (t.def < 0) t.def = 1;
        t.mag -= pg->val.equip->equip[pos]->mod.mag;
        if (t.mag < 0) t.mag = 1;
        t.spr -= pg->val.equip->equip[pos]->mod.spr;
        if (t.spr < 0) t.spr = 1;
    }
    return t;
}