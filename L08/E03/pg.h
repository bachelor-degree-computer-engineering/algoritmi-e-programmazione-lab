#ifndef APALAB_PG_H
#define APALAB_PG_H

#include "inventario.h"

typedef enum{
    aggiungi, rimuovi
}sel;

typedef struct{
    int inUso;
    inv_t **equip;
}tabEquip_t;

typedef struct{
    char codice[7];
    char *nome;
    char *classe;
    tabEquip_t *equip;
    stat_t statbase;
    stat_t cstat;
}pg_t;

typedef struct node *link, nodePG;

struct node{
    pg_t val;
    link next;
};

typedef struct{
    link headPG;
    link tailPG;
    int nPG;
}tabPG;

link creaPG(pg_t val, link next);
int TailIns(tabPG* tab,pg_t val);
tabPG *creatabPG();
void distruggitabPG(tabPG *tab);
tabPG *caricatabPG(FILE *in);
tabPG *aggiungiPG(tabPG *tab);
tabPG *eliminaPG(tabPG *tab);
void pgprint(FILE *out, pg_t tmp);
void stampatabPG(FILE *out, tabPG *tabp);
tabInv *creaInventario(FILE *in);
void stampaInv(FILE *out, tabInv *tabInv1);
tabEquip_t *creaEquip();
void distruggiEquip(tabEquip_t *equip);
link ricercaPG(char cod[7],tabPG *tabellapg);
void aggiungiEquip(link pg, tabInv *tabellainv);
void rimuoviEquip(link pg, tabInv *tabellainv);
stat_t aggiornaStat(link pg, int pos, sel tipo);
#endif //APALAB_PG_H
