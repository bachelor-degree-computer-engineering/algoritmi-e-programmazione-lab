#include <stdio.h>
#include <stdlib.h>

typedef struct{
    int s,f,d;
}att;

void quicksort(att *val, int n);
void stampaatt(att *val, int n);
int attsel(att *val, int n);
int attselR(att *val, int i);
int attselPD(att *val, int n);

int main() {
    FILE *in;
    int n,i;
    att *val;
    in = fopen("att.txt","r");
    fscanf(in,"%d",&n);
    val = malloc(n* sizeof(att));
    for(i=0;i<n;i++){
        fscanf(in,"%d %d",&val[i].s,&val[i].f);
        val[i].d = val[i].f - val[i].s;
    }
    quicksort(val, n);
    stampaatt(val, n);
    printf("Soluzione Ricorsiva - Durata max: %d\n",attsel(val, n));
    printf("\nSoluzione PD - Durata max: %d",attselPD(val, n));
    return 0;
}

void swap(att *val, int a, int b){
    att tmp;
    tmp = val[a];
    val[a] = val[b];
    val[b] = tmp;
}

int compatibili(att i, att j){
    if(i.s < j.f && j.s < i.f) return 0;
    return 1;
}


int partition(att *val, int l, int r){
    int i = l-1, j = r;
    att x = val[r];
    for( ; ; ){
        while(val[++i].f < x.f);
        while(val[--j].f > x.f)
            if(j == l)
                break;
        if(i >= j)
            break;
        swap(val,i,j);
    }
    swap(val, i, r);
    return i;
}

void quicksortR(att *val, int l, int r){
    int q;
    if(r <= l)
        return;
    q = partition(val, l, r);
    quicksortR(val, l, q-1);
    quicksortR(val, q+1, r);
}

void quicksort(att *val, int n){
    int l=0, r=n-1;
    quicksortR(val,l,r);
}

void stampaatt(att *val, int n){
    int i;
    for(i=0; i<n; i++)
        printf("(%d %d) ",val[i].s,val[i].f);
    printf("\n");
}

int attsel(att *val, int n){
    int i, d = 0, tmp;
    for(i=0; i<n; i++) {
        tmp = attselR(val, i);
        if(tmp > d)
            d = tmp;
    }
    return d;
}

int max(int a, int b){
    if(a > b) return a;
    return b;
}

int attselR(att *val, int i){
    int j, ris;
    ris = val[i].d;
    for(j=0; j<i; j++){
        if(compatibili(val[i], val[j]))
            ris = max(ris, val[i].d + attselR(val, j));
    }
    return ris;
}

void displaysol(att *val, int *p, int i){
    if(p[i] == -1){
        printf("(%d %d) ", val[i].s, val[i].f);
        return;
    }
    displaysol(val, p, p[i]);
    printf("(%d %d) ", val[i].s, val[i].f);
}

int attselPD(att *val, int n){
    int i, j, ris = 0 , *l, *p, last = 1;
    l = malloc(n* sizeof(int));
    p = malloc(n* sizeof(int));
    l[0] = val[0].d;
    p[0] = -1;
    for(i=1; i<n; i++){
        p[i] = -1;
        l[i] = l[i-1];
        for(j=0; j<i; j++){
            if(compatibili(val[i],val[j]) && (l[j]+val[i].d)>l[j]) {
                l[i] = l[j] + val[i].d;
                p[i] = j;
            }
            if(l[i] > ris){
                ris = l[i];
                last = i;
            }
        }
    }
    displaysol(val, p, last);
    free(l);
    free(p);
    return ris;
}