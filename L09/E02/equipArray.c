#include "equipArray.h"

struct equipArray_s{
    int inUso;
    int vetEq[8];
};

equipArray_t equipArray_init(){
    equipArray_t tmp;
    int i;
    tmp = malloc(sizeof(*tmp));
    tmp->inUso = 0;
    for(i=0; i<8;i++)
        tmp->vetEq[i] = -1;
    return tmp;
}

void equipArray_free(equipArray_t equipArray){
    free(equipArray);
}

int equipArray_inUse(equipArray_t equipArray){
    return equipArray->inUso;
}

void equipArray_print(FILE *fp, equipArray_t equipArray, invArray_t invArray){
    int i;
    for(i=0;i<8;i++){
        invArray_printByIndex(fp,invArray,equipArray->vetEq[i]);
    }
}

void equipArray_update(equipArray_t equipArray, invArray_t invArray){
    int i,pos;
    char nome[50];
    printf("Inserisci il nome dell'oggetto da equipaggiare: ");
    scanf("%s ", nome);
    pos = invArray_searchByName(invArray, nome);
    if(pos == -1){
        printf("Elemento non trovato;");
        return;
    }
    for(i=0; i<8; i++){
        if(equipArray->vetEq[i] == -1){
            equipArray->vetEq[i] = pos;
            equipArray->inUso++;
            return;
        }
    }
    printf("Elemento non inserito: inventario pieno");
}

int equipArray_getEquipByIndex(equipArray_t equipArray, int index){
    return equipArray->vetEq[index];
}
