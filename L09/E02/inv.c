#include "inv.h"

void stat_read(FILE *fp, stat_t *statp){
    fscanf(fp,"%d %d %d %d %d %d\n",&(statp->hp),&(statp->mp),&(statp->atk),&(statp->def),&(statp->mag),&(statp->spr));
}

void stat_print(FILE *fp, stat_t *statp, int soglia){
    if(statp->hp <= soglia || statp->mp <= soglia || statp->atk <= soglia || statp->def <= soglia || statp->mag <= soglia || statp->spr<= soglia)
        fprintf(fp, "%d %d %d %d %d %d\n",1,1,1,1,1,1);
    else
        fprintf(fp, "%d %d %d %d %d %d\n",statp->hp,statp->mp,statp->atk,statp->def,statp->mag,statp->spr);
    return;
}
stat_t inv_getStat(inv_t *invp){
    return invp->stat;
}

void inv_read(FILE *fp, inv_t *invp){
    fscanf(fp,"%s %s",invp->nome,invp->tipo);
    stat_read(fp,&(invp->stat));
    return;
}

void inv_print(FILE *fp, inv_t *invp){
    fprintf(fp,"%s %s ",invp->nome,invp->tipo);
    stat_print(fp, &(invp->stat),0);
}

stat_t statsetvoid(){
    stat_t t;
    t.hp = t.mp = t.atk = t.def = t.mag = t.spr = 0;
    return t;
}

stat_t updateStat(stat_t base, stat_t add){
    base.hp += add.hp;
    base.mp += add.mp;
    base.atk += add.atk;
    base.def += add.def;
    base.mag += add.mag;
    base.spr += add.spr;
    return base;
}