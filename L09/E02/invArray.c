#include "invArray.h"

struct invArray_s{
    inv_t *vettInv;
    int ninv;
    int maxinv;
};

invArray_t invArray_init(){
    invArray_t tmp;
    tmp = malloc(sizeof(*tmp));
    return tmp;
}

void invArray_free(invArray_t invArray){
    free(invArray);
}

void invArray_read(FILE *fp, invArray_t invArray){
    int i;
    fscanf(fp, "%d", &(invArray->ninv));
    invArray->vettInv = malloc(invArray->ninv* sizeof(inv_t));
    for(i=0; i<invArray->ninv;i++){
        inv_read(fp, &(invArray->vettInv[i]));
    }
}

void invArray_print(FILE *fp, invArray_t invArray){
    int i;
    for(i=0;i<invArray->ninv;i++){
        inv_print(fp, &(invArray->vettInv[i]));
    }
}

void invArray_printByIndex(FILE *fp, invArray_t invArray, int index){
    inv_print(fp, &(invArray->vettInv[index]));
}

inv_t *invArray_getByIndex(invArray_t invArray, int index){
    return &(invArray->vettInv[index]);
}

int invArray_searchByName(invArray_t invArray, char *name) {
    int i;
    for (i = 0; i < invArray->ninv; i++) {
        if (strcmp(name, invArray->vettInv[i].nome) == 0)
            return i;
    }

    return -1;
}