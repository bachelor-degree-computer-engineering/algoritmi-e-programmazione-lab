#include "pg.h"

int pg_read(FILE *fp, pg_t *pgp){
    if(fscanf(fp,"%s %s %s",pgp->cod,pgp->nome,pgp->classe) != 3)
        return 0;
    pgp->equip = NULL;
    stat_read(fp, &(pgp->b_stat));
    pgp->eq_stat = statsetvoid();
    return 1;
}

void pg_clean(pg_t *pgp){
    equipArray_free(pgp->equip);
}

void pg_print(FILE *fp, pg_t *pgp, invArray_t invArray){
    fprintf(fp, "%s %s %s",pgp->cod,pgp->nome,pgp->classe);
    stat_print(fp,&(pgp->b_stat),0);
    printf("Equipaggiamento:\n");
    stat_print(fp,&(pgp->eq_stat),0);

    if(pgp->equip != NULL) {
        printf("Equipaggiamento:\n");
        equipArray_print(fp, pgp->equip, invArray);
    }
}

void pg_updateEquip(pg_t *pgp, invArray_t invArray){
    int i,index=0;
    inv_t *tmp;
    stat_t statToAdd;
    equipArray_update(pgp->equip,invArray);
    index = equipArray_inUse(pgp->equip) - 1;
    tmp = invArray_getByIndex(invArray, index);
    statToAdd = inv_getStat(tmp);
    updateStat(pgp->eq_stat,statToAdd);
}
