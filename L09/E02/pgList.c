#include "pgList.h"

typedef struct node *link;
struct node{
    pg_t val;
    link next;
};

struct pgList_s{
    link headPG;
    int n;
};

pgList_t pgList_init(){
    pgList_t tmp;
    tmp = malloc(sizeof(*tmp));
    tmp->headPG = NULL;
    tmp->n = 0;
    return tmp;
}

void pgList_free(pgList_t pgList){
    //Cancello prima lista;
    free(pgList);
}

void pgList_read(FILE *fp, pgList_t pgList){
    pg_t pg;
    while(pg_read(fp,&pg)){
        pgList_insert(pgList,pg);
    }
}

void pgList_print(FILE *fp, pgList_t pgList, invArray_t invArray){
    link x;
    for(x=pgList->headPG; x!=NULL; x=x->next){
        pg_print(fp,&(x->val),invArray);
    }
}

link newNode(pg_t val, link next){
    link x;
    x = malloc(sizeof(*x));
    x->val = val;
    x->next = next;
    return x;
}

void pgList_insert(pgList_t pgList, pg_t pg){
    pgList->headPG = newNode(pg, pgList->headPG);
}

void pgList_remove(pgList_t pgList, char* cod){
    link x,p;
    for(p=NULL,x=pgList->headPG; x!=NULL;p=x, x=x->next){
        if(strcmp(cod, x->val.cod)==0){
            if(x == pgList->headPG)
                pgList->headPG = x->next;
            else
                p->next = p->next->next;
            free(x);
            return;
        }
    }
}

pg_t *pgList_searchByCode(pgList_t pgList, char* cod){
    link x;
    for(x=pgList->headPG;x!=NULL; x=x->next){
        if(strcmp(cod, x->val.cod) == 0)
            return &(x->val);
    }
}