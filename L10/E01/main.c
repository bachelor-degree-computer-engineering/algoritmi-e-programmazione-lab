#include <stdio.h>
#include <stdlib.h>
#define TEST_SET "hard_test_set.txt"

int generaCollane(int z, int r, int t, int s);
int fz(int ****M, int z, int r, int t, int s);
int fr(int ****M, int z, int r, int t, int s);
int ft(int ****M, int z, int r, int t, int s);
int fs(int ****M, int z, int r, int t, int s);

int main(){
    int num_testset, z,r,t,s,i;
    FILE *in;
    in = fopen(TEST_SET, "r");
    fscanf(in, "%d", &num_testset);
    for(i=0; i<num_testset; i++){
        fscanf(in, "%d %d %d %d", &z,&r,&t,&s);
        printf("[%d] Z = %d, R=%d, T=%d, S=%d. TOT=%d MAX: %d\n",i+1,z,r,t,s,z+r+t+s,generaCollane(z,r,t,s));
    }
    fclose(in);
    return 0;
}
int max(int a, int b){
    if(a > b)
        return a;
    return b;
}

int fz(int ****M,int z, int r, int t, int s) {
    if(z == 0)
        return 0;
    z--;
    if(M[z][r][t][s] != -1)
        return M[z][r][t][s];
    M[z][r][t][s] = max(1+fz(M,z,r,t,s),1+fr(M,z,r,t,s));
    return M[z][r][t][s];
}

int fr(int ****M,int z, int r, int t, int s){
    if(r == 0)
        return 0;
    r--;
    if(M[z][r][t][s] != -1)
        return M[z][r][t][s];
    M[z][r][t][s] = max(1+fs(M,z,r,t,s),1+ft(M,z,r, t, s));
    return M[z][r][t][s];
}

int fs(int ****M,int z, int r, int t, int s){
    if(s == 0)
        return 0;
    s--;
    if(M[z][r][t][s] != -1)
        return M[z][r][t][s];
    M[z][r][t][s] = max(1+fs(M,z,r,t,s),1+ft(M,z,r, t,s));
    return  M[z][r][t][s];
}

int ft(int ****M,int z, int r, int t, int s){
    if(t==0)
        return 0;
    t--;
    if(M[z][r][t][s] != -1)
        return M[z][r][t][s];
    M[z][r][t][s] =  max(1+fz(M,z,r,t,s),1+fr(M,z,r, t,s));
    return M[z][r][t][s];
}
int generaCollane(int z, int r, int t, int s){
    int l = 0,lz,lr,lt,ls,*****M,i,j,k,c,p;
    M = malloc(4* sizeof(int****));
    for(i=0; i<4; i++){
        M[i] = malloc((z+1)* sizeof(int***));
        for(j=0; j<z+1; j++) {
            M[i][j] = malloc((r+1) * sizeof(int **));
            for (k = 0; k < r+1; k++){
                M[i][j][k] = malloc((t+1) * sizeof(int*));
                for(c = 0; c<t+1; c++){
                    M[i][j][k][c] = malloc(s+1 * sizeof(int));
                }
            }
        }
    }
    for(p=0; p<4;p++)
        for(i=0; i<=z; i++)
            for(j=0; j<=r; j++)
                for(k=0; k<=t; k++)
                    for(c=0; c<=s; c++)
                        M[p][i][j][k][c] = -1;
    lz = fz(M[0],z,r,t,s);
    lr = fr(M[1],z,r,t,s);
    l = max(lz,lr);
    lt = ft(M[2],z,r,t,s);
    l = max(l, lt);
    ls = fs(M[3],z,r,t,s);
    l = max(l,ls);
    for(i=0; i<4; i++){
        for(j=0; j<=z; j++){
            for(k=0; k<=r; k++){
                for(c = 0; c <= t; c++){
                    free(M[i][j][k][c]);
                }
                free(M[i][j][k]);
            }
            free(M[i][j]);
        }
        free(M[i]);
    }
    free(M);
    return l;
}