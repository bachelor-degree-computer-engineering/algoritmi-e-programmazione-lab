//TODO codice che calcola prima il numero di diagonali ammissibili, successivamente le momorizza in una struttura dati apposita. Potrebbe essere una matrice che fa
// riferimento agli indici nel vettore elementi.
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define DD 12
#define DP 20

typedef struct{
    char *nome;
    int tipo,enter,exit, start,finish,diff;
    float val;
}elemento;

//void generaProgramma(elemento *elementi, int n);
//void generaProgrammaR(int pos, elemento *elementi, int **sol, int *lsol, int n);
int generaDiagonali(elemento *elementi, int n);
int generaDiagonaliR(int pos, elemento *elementi, int *sol, int l, int n);

int main(){
    elemento *elementi;
    FILE *in;
    int i,n;
    char nome[101];
    in = fopen("elementi.txt","r");
    fscanf(in,"%d",&n);
    elementi = malloc(n* sizeof(elemento));
    for(i=0;i<n;i++) {
        fscanf(in, "%s %d %d %d %d %d %f %d",nome,&elementi[i].tipo,&elementi[i].enter,&elementi[i].exit,&elementi[i].start,&elementi[i].finish,&elementi[i].val,&elementi[i].diff);
        elementi[i]. nome = strdup(nome);
    }
    printf("Generate %d diagonali possibili.", generaDiagonali(elementi,n));
    return 0;
}

int generaDiagonali(elemento *elementi, int n){
    int l, count = 0, sol[5];
    for(l=1; l<5; l++) {
        count += generaDiagonaliR(0, elementi, sol, n);
    }
    return count;
}


int generaDiagonaliR(int pos,elemento *elementi, int *sol,int l, int n){
    int i,acrobatico,tmpdiff;
    if(pos == l)
        return count+1;
    for(i=0; i<n; i++){
        if(pos == 0){
            if(elementi[i].enter == 1 && elementi[i].start == 0)
                sol[pos] = i;
        }
        else {
            if (elementi[i].enter != elementi[sol[pos - 1]].finish) {
                sol[pos] = i;
            }
            if (pos == l - 1) {
                if (elementi[i].finish == 1)
                    sol[pos] = i;
            }
        }
        generaDiagonaliR(pos+1, elementi,sol,l,n);
    }
}