//TODO valutazione di vincoli dinamici al problema

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define DD 12
#define DP 20

typedef struct{
    char *nome;
    int tipo,enter,exit, start,finish,diff;
    float val,prob;
}elemento;

void generaProgramma(elemento *elementi, int n);

int main(){
    elemento *elementi;
    FILE *in;
    int i,n;
    char nome[101];
    in = fopen("elementi.txt","r");
    fscanf(in,"%d",&n);
    elementi = malloc(n* sizeof(elemento));
    for(i=0;i<n;i++) {
        fscanf(in, "%s %d %d %d %d %d %f %d",nome,&elementi[i].tipo,&elementi[i].enter,&elementi[i].exit,&elementi[i].start,&elementi[i].finish,&elementi[i].val,&elementi[i].diff);
        elementi[i]. nome = strdup(nome);
        elementi[i].prob = elementi[i].val/elementi[i].diff;
    }
    generaProgramma(elementi, n);
    return 0;
}

void swap(elemento *v, int n1, int n2){
    elemento temp;
    temp  = v[n1];
    v[n1] = v[n2];
    v[n2] = temp;
}

static int partition(elemento *v, int l, int r){
    int i=l-1, j = r;
    elemento x;
    x = v[r];
    for( ; ; ){
        while(v[++i].prob < x.prob);
        while(v[--j].prob > x.prob)
            if(j == l)
                break;
        if(i >= j)
            break;
        swap(v,i,j);
    }
    swap(v,i,r);
    return i;
}

static void quicksortR(elemento *v, int l, int r){
    int q;
    if(r <= l)
        return;
    q = partition(v,l,r);
    quicksortR(v,l,q-1);
    quicksortR(v,q+1,r);
}

void sort(elemento *v, int n){
    int l=0, r=n-1;
    quicksortR(v,l,r);
}

void generaProgramma(elemento *elementi, int n){
    int sol[3][5], l[3];
    sort(elementi,n);
    //parto dalla terza diagonale, piazzando l'elemento a densità maggiore, e piazzando due elementi acrobatici in sequenza.

}

