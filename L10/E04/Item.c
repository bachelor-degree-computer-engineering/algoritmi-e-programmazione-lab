#include "Item.h"
#include <string.h>
#include <stdlib.h>

Item itemCreate(char *nome, char *rete){
    Item t;
    t.nome = strdup(nome);
    t.rete = strdup(rete);
    return t;
}

void itemFree(Item t){
   free(t.nome);
   free(t.rete);
}

