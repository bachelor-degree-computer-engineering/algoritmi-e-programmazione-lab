// Modulo Item: contiene una stringa per il nome dell'elaboratore e una
// stringa per il nome della rete
//

#ifndef GRAFO_ITEM_H
#define GRAFO_ITEM_H

typedef struct{
    char *nome;
    char *rete;
}Item;

Item itemCreate(char *nome, char *rete);
void itemFree(Item t);

#endif //GRAFO_ITEM_H
