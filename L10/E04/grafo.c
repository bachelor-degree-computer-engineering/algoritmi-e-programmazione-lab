#include <stdlib.h>
#include <stdio.h>
#include "grafo.h"

typedef struct node *link;
struct node{int id; int wt; link next;};
struct grafo{int V,E; int **madj; link *ladj;int ladjgen;};

static link NEW(int dst, int w, link next);
static Edge EdgeCreate(int v, int w, int wt);
static void insertE(Graph g, Edge e);

static link NEW(int dst, int w, link next){
    link x;
    x = malloc(sizeof(*x));
    x->id = dst;
    x->wt = w;
    x->next =  next;
    return x;
}

void printE(Graph G){
    printf("E:%d\n",G->E);
}
Graph GRAPHinit(int V){
    int i;
    Graph g;
    g = malloc(sizeof(*g));
    g->V = V;
    g->E = 0;
    g->ladjgen = 0;
    g->madj = malloc(V* sizeof(int*));
    for(i=0; i<V; i++)
        g->madj[i] = calloc(V, sizeof(int));
    return g;
}

void GRAPHfree(Graph g){
    int i;
    for(i=0; i < g->V; i++) {
        free(g->madj[i]);
    }
    free(g->madj);
    free(g);
}

static Edge EdgeCreate(int v, int w, int wt){
    Edge e;
    e.v = v;
    e.w = w;
    e.wt = wt;
    return e;
}

void GRAPHinsertE(Graph G, int id1, int id2, int wt){
    insertE(G, EdgeCreate(id1, id2, wt));
}

static void insertE(Graph g, Edge e){
    int v = e.v, w = e.w, wt = e.wt;
    if(g->madj[v][w] == 0)
        g->E++;
    g->madj[v][w] = wt;
    g->madj[w][v] = wt;
}

void GRAPHprintmatrix(Graph G){
    int i,j;
    for(i=0; i<G->V; i++){
        for(j=0; j <G->V; j++)
            printf("%3d ",G->madj[i][j]);
        printf("\n");
    }
}

static link listIns(link head, int dst, int wt){
     head = NEW(dst, wt ,head);
     return head;
}

void GRAPHladjcreate(Graph G){
    int i,j;
    if(G->ladjgen ==0) {
        G->ladjgen = 1;
        G->ladj = malloc(G->V * sizeof(link));
        for (i = 0; i < G->V; i++)
            G->ladj[i] = NULL;
        for (i = 0; i < G->V; i++) {
            for (j = 0; j < G->V; j++) {
                if (G->madj[i][j] != 0) {
                    G->ladj[i] = listIns(G->ladj[i], j, G->madj[i][j]);
                }
            }
        }
    }
}

void GRAPHprintlist(Graph G){
    int i;
    link x;
    if(G->ladjgen == 1) {
        for (i = 0; i < G->V; i++) {
            if (G->ladj[i] != NULL) {
                printf("-%d -> ", i);
                for (x = G->ladj[i]; x->next != NULL; x = x->next) {
                    printf("%d/%d ->", x->id, x->wt);
                }
                printf("%d/%d\n", x->id, x->wt);
            }
        }
    }
    else{
        printf("Lista non ancora generata.\n");
    }
}
static int LISTsearch(link head, int i){
    link x;
    for(x=head; x!=NULL; x=x->next)
        if(x->id == i)
            return 1;
    return 0;
}
static int Graphadjmatrix(Graph G, int i1, int i2, int i3){
    return (G->madj[i1][i2] != 0 && G->madj[i2][i3] != 0 && G->madj[i1][i3] != 0);
}

static int Graphadjlist(Graph G, int i1, int i2, int i3){
    return (LISTsearch(G->ladj[i1],i2) && LISTsearch(G->ladj[i2],i3) && LISTsearch(G->ladj[i3],i1));
};

void GRAPHadj(Graph G,int i1, int i2,int i3) {
    #ifdef LIST
    if (Graphadjlist(G,i1,i2,i3))
    #endif
    #ifndef LIST
    if(Graphadjmatrix(G,i1,i2,i3))
    #endif
        printf("I vertici formano un sottografo connesso.\n");
    else
        printf("I vertici non formano un sottografo connesso.\n");
}

int GRAPHedges(Graph G, int *v, int i){
    int j,n=0;
    for(j=0; j<G->V; j++)
        if(G->madj[i][j] != 0)
            v[n++] = j;
    return n;
}