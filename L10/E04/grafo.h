#ifndef GRAFO_GRAFO_H
#define GRAFO_GRAFO_H

typedef struct{int v, w, wt;}Edge;
typedef struct grafo *Graph;

Graph GRAPHinit(int V);
void GRAPHfree(Graph t);
void GRAPHinsertE(Graph G, int id1, int id2, int wt);
void GRAPHladjcreate(Graph G);
void GRAPHprintmatrix(Graph G);
void GRAPHprintlist(Graph G);
int GRAPHedges(Graph G, int *v, int i);
void printE(Graph G);
void GRAPHadj(Graph G, int i1, int i2,int i3);
#endif //GRAFO_GRAFO_H
