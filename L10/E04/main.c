#include <stdio.h>
#include <stdlib.h>

#include "st.h"
#include "grafo.h"

#define MAXL 31
Graph read_file(FILE *in, ST *st, int *V);
void printMenu();

int main(int argc, char **argv) {
    FILE *in;
    ST table = NULL;
    Graph G;
    char v1[31],v2[31],v3[31];
    int V = 0,continua = 1, scelta,i,j,i1,i2,i3,n,*v;
    in = fopen("grafo.txt", "r");
    G = read_file(in, &table, &V);
    printf("Caricati %d vertici:\n",V);
    printE(G);
    STprint(table);
    GRAPHprintmatrix(G);
    printf("Grafo caricato in memoria\n");
    while(continua){
        printMenu();
        scanf("%d", &scelta);
        switch(scelta){
            case 0:
                continua = 0;
                break;
            case 1:
                GRAPHprintmatrix(G);
                break;
            case 2:
                printf("Genero lista delle adiacenze...\n");
                GRAPHladjcreate(G);
                break;
            case 3:
                GRAPHprintlist(G);
                break;
            case 4:
                printf("Inserisci i nomi di 3 vertici:");
                scanf("%s %s %s",v1,v2,v3);
                i1 = STsearchbyname(table, v1);
                i2 = STsearchbyname(table, v2);
                i3 = STsearchbyname(table, v3);
                GRAPHadj(G,i1,i2,i3);
            case 5:
                printf("Stampo lista dei vertici in ordine alfabetico: \n");
                v = malloc(V * sizeof(int));
                for(i=0; i < V; i++){
                    printf("-(%s %s)\n",STgetbyindex(table,i).nome, STgetbyindex(table,i).rete);
                    n = GRAPHedges(G, v, i);
                    for(j=0; j<n; j++)
                        printf("   -(%s %s)\n",STgetbyindex(table,v[j]).nome, STgetbyindex(table,v[j]).rete);
                }
                free(v);
                break;
            default:
                printf("riprova:\n");
                break;
        }
    }
    GRAPHfree(G);
    STfree(table);
    return 0;
}

Graph read_file(FILE *in, ST *st, int *V){
    Graph G;
    char id1[MAXL],id2[MAXL], rete1[MAXL], rete2[MAXL];
    Item x1,x2;
    int i,i1,i2,wt,n = 0;
    while(fscanf(in,"%s %s %s %s %d\n",id1,rete1,id2,rete2,&wt) == 5) n++;
    rewind(in);
    *st = STinit(n);
    while(fscanf(in,"%s %s %s %s %d\n",id1,rete1,id2,rete2,&wt) == 5){
        x1 = itemCreate(id1, rete1);
        x2 = itemCreate(id2, rete2);
        i1 = STsearchbyname(*st, id1);
        i2 = STsearchbyname(*st,id2);
        if(i1 == -1) {
            STinsert(*st, x1);
            (*V)++;
        }
        if(i2 == -1) {
            STinsert(*st, x2);
            (*V)++;
        }
    }
    STsort(*st);
    rewind(in);
    G = GRAPHinit(*V);
    while(fscanf(in, "%s %s %s %s %d\n",id1, rete1, id2, rete2, &wt) == 5){
        i1 = STsearchbyname(*st, id1);
        i2 = STsearchbyname(*st, id2);
        GRAPHinsertE(G, i1, i2, wt);
    }
    return G;
}

void printMenu(){
    printf("-0 uscita\n");
    printf("-1 stampa Matrice delle adiacenze\n");
    printf("-2 Genera lista delle adiacenze\n");
    printf("-3 stampa lista delle adiacenze\n");
    printf("-4 Verifica se 3 vertici formano un sottografo connesso\n");
    printf("-5 Stampa i vertici in ordine alfabetico\n");
    printf(">");
}