#include "Item.h"
#include <string.h>
#include <stdlib.h>

Item itemCreate(char *nome){
    Item t;
    t.nome = strdup(nome);
    return t;
}

void itemFree(Item t){
   free(t.nome);
}

