#ifndef GRAFO_ITEM_H
#define GRAFO_ITEM_H

typedef struct{
    char *nome;
}Item;

Item itemCreate(char *nome);
void itemFree(Item t);

#endif //GRAFO_ITEM_H
