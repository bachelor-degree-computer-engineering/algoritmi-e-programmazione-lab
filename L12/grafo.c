#include <stdlib.h>
#include <stdio.h>
#include "grafo.h"
#include "ST.h"

//This value is a sort of minus infinity
#define MINWT -10000

typedef struct node *link;
struct node{int id; int wt; link next;};
struct grafo{int V,E; int **madj; link *ladj;int ladjgen; ST table; int *sourcenode;};

static link NEW(int dst, int w, link next);
static Edge EdgeCreate(int v, int w, int wt);
static void insertE(Graph g, Edge e);
static void removeE(Graph G, Edge e);

static link NEW(int dst, int w, link next){
    link x;
    x = malloc(sizeof(*x));
    x->id = dst;
    x->wt = w;
    x->next =  next;
    return x;
}

int GRAPHgetE(Graph G){
    return G->E;
}

int GRAPHgetV(Graph G){
    return G->V;
}

Graph GRAPHinit(int V){
    int i;
    Graph g;
    g = malloc(sizeof(*g));
    g->V = V;
    g->E = 0;
    g->ladjgen = 0;
    g->madj = malloc(V* sizeof(int*));
    for(i=0; i<V; i++)
        g->madj[i] = calloc(V, sizeof(int));
    g->sourcenode = calloc(V, sizeof(int));
    return g;
}

void GRAPHfree(Graph g){
    int i;
    for(i=0; i < g->V; i++) {
        free(g->madj[i]);
    }
    free(g->madj);
    free(g);
}

static Edge EdgeCreate(int v, int w, int wt){
    Edge e;
    e.v = v;
    e.w = w;
    e.wt = wt;
    return e;
}

void GRAPHinsertE(Graph G, int id1, int id2, int wt){
    insertE(G, EdgeCreate(id1, id2, wt));
}

void GRAPHremoveE(Graph G, int id1, int id2, int wt){
    removeE(G,EdgeCreate(id1,id2, wt));
}

Graph readFile(FILE *in){
    int i,n,j,wt;
    char nome[31],id1[30], id2[30];
    fscanf(in, "%d\n",&n);
    Graph G = GRAPHinit(n);
    ST st;
    st = STinit(n);
    for(i=0; i<n; i++){
        fscanf(in,"%s\n", nome);
        STinsert(st, itemCreate(nome));
    }
    while(fscanf(in,"%s %s %d\n",id1,id2,&wt ) == 3){
        i = STsearchbyname(st, id1);
        j = STsearchbyname(st, id2);
        GRAPHinsertE(G,i,j,wt);
    }
    G->table = st;
    return G;
}

Graph GRAPHreadFile(FILE *in){
    return readFile(in);
}

static void removeE(Graph G, Edge e){
    int v = e.v, w = e.w;
    if(G->madj[v][w] != 0){
        G->madj[v][w] = 0;
        G->E--;
    }
}

static void insertE(Graph g, Edge e){
    int v = e.v, w = e.w, wt = e.wt;
    if(g->madj[v][w] == 0)
        g->E++;
    g->madj[v][w] = wt;
}

void GRAPHprintmatrix(Graph G){
    int i,j;
    for(i=0; i<G->V; i++){
        for(j=0; j <G->V; j++)
            printf("%3d ",G->madj[i][j]);
        printf("\n");
    }
}

static link listIns(link head, int dst, int wt){
     head = NEW(dst, wt ,head);
     return head;
}

void GRAPHladjcreate(Graph G){
    int i,j;
    if(G->ladjgen ==0) {
        G->ladjgen = 1;
        G->ladj = malloc(G->V * sizeof(link));
        for (i = 0; i < G->V; i++)
            G->ladj[i] = NULL;
        for (i = 0; i < G->V; i++) {
            for (j = 0; j < G->V; j++) {
                if (G->madj[i][j] != 0) {
                    G->ladj[i] = listIns(G->ladj[i], j, G->madj[i][j]);
                }
            }
        }
    }
}

void GRAPHprintlist(Graph G){
    int i;
    link x;
    if(G->ladjgen == 1) {
        for (i = 0; i < G->V; i++) {
            if (G->ladj[i] != NULL) {
                printf("-%d -> ", i);
                for (x = G->ladj[i]; x->next != NULL; x = x->next) {
                    printf("%d/%d ->", x->id, x->wt);
                }
                printf("%d/%d\n", x->id, x->wt);
            }
        }
    }
    else{
        printf("Lista non ancora generata.\n");
    }
}
static int LISTsearch(link head, int i){
    link x;
    for(x=head; x!=NULL; x=x->next)
        if(x->id == i)
            return 1;
    return 0;
}

int GRAPHedges(Graph G, int *v, int i){
    int j,n=0;
    for(j=0; j<G->V; j++)
        if(G->madj[i][j] != 0)
            v[n++] = j;
    return n;
}

void dfsR(Graph G, Edge e, int *time, int *pre, int *flag){
    link t;
    int v, w= e.w;
    if(e.v != e.w)
        printf("");
    pre[w] = (*time)++;
    for(t = G->ladj[w]; t != NULL; t=t->next)
        if(pre[t->id] == -1)
            dfsR(G,EdgeCreate(w,t->id,1), time, pre, flag);
        else{
            v = t->id;
            if(pre[w] < pre[v])
                *flag = 0;
        }
}

static int isAciclico(Graph G){
    int isAciclico = 1, v, time=0, *pre;
    pre = malloc(G->V * sizeof(int));
    for(v=0; v < G->V; v++)
        pre[v] = -1;
    for(v=0; v<G->V; v++)
        if(pre[v] == -1)
            dfsR(G,EdgeCreate(v,v,1), &time, pre, &isAciclico);
    return isAciclico;
}

static int checkSum(Edge *sol, int l){
    int i, sum=0;
    for(i=0; i<l; i++){
        sum += sol[i].wt;
    }
    return sum;
}

static void combinazioni(Graph G,int pos, Edge *sol, Edge *bestsol, Edge *archi,  int *bestval, int l, int start, int *trovata){
    int i, tmpval;
    if(pos >= l){
        for(i=0; i<l; i++){
            removeE(G,sol[i]);
        }
        if(isAciclico(G)){
            *trovata = 1;
            tmpval = checkSum(sol, pos);
            if(tmpval > *bestval) {
                for (i = 0; i < l; i++)
                    bestsol[i] = sol[i];
                *bestval = tmpval;
            }
        }
        for(i=0; i<l; i++){
            insertE(G,sol[i]);
        }
        return;
    }
    for(i = start; i<G->E; i++){
        sol[pos] = archi[i];
        combinazioni(G, pos+1, sol, bestsol, archi, bestval, l, i+1, trovata);
    }
    return;
}

static Edge *createEdgelist(Graph G){
    Edge *archi;
    archi = malloc(G->E* sizeof(Edge));
    link t;
    int i,j=0;
    for(i=0; i<G->V; i++){
        for(t = G->ladj[i]; t!=NULL; t=t->next){
            archi[j++] = EdgeCreate(i, t->id, t->wt);
        }
    }
    return archi;
}

void GRAPHtoDAG(Graph G){
    Edge *sol, *bestsol, *archi;
    int i,j, trovata = 0,bestval = 0;
    sol = malloc(G->E* sizeof(Edge));
    bestsol = malloc(G->E* sizeof(Edge));
    GRAPHladjcreate(G);
    archi = createEdgelist(G);
    for(i=0; i<G->E; i++)
        printf("(%s -> %s)\n", STgetbyindex(G->table,archi[i].v).nome,STgetbyindex(G->table,archi[i].w).nome);
    for(i=1; i<G->E && trovata == 0; i++){
        combinazioni(G,0,sol,bestsol, archi,&bestval, i, 0, &trovata);
    }
    if(trovata) {
        printf("DAG ottenuto rimuovendo %d  archi a peso %d: \n", i-1, bestval);
        for (j = 0; j < i-1; j++) {
            printf("(%s -> %s) ", STgetbyindex(G->table,bestsol[j].v).nome,STgetbyindex(G->table,bestsol[j].w).nome);
            removeE(G,bestsol[j]);
        }
        printf("\n");
    }
}

void DAGprintSource(Graph G){
    int *in_degree, i,j;
    in_degree = calloc(G->V, sizeof(int));
    for(i=0; i<G->V; i++){
        for(j=0; j<G->V; j++)
            if(G->madj[i][j] != 0)
               in_degree[j]++;
    }
    for(i=0; i<G->V; i++){
        if(in_degree[i]==0){
            printf("%s ", STgetbyindex(G->table, i).nome);
            G->sourcenode[i] = 1;
        }
    }
}
void DAGTSR(Graph G, int v, int *ts, int *pre, int *time){
    int w;
    pre[v] = 0;
    for(w=0; w <G->V; w++)
        if(G->madj[w][v] != 0)
            if(pre[w] == -1)
                DAGTSR(G, w, ts, pre, time);
    ts[(*time)++] = v;
}
static void ordinamentoTopologico(Graph G, int *ts){
    int v,k=0, i, time=0,  *pre;
    link t;
    pre = malloc(G->V* sizeof(int));
    for(v=0; v<G->V; v++)
        pre[v] = ts[v] = -1;
    for(v=0; v<G->V; v++)
        if(pre[v] == -1)
            DAGTSR(G, v, ts, pre, &time);
}

static void LongestPath(){

}

void DAGlongestPathS(Graph G){
    int i, *ts, *dist, v, w, j , k;
    dist = malloc(G->V* sizeof(int));
    ts = malloc(G->V* sizeof(int));
    ordinamentoTopologico(G,ts);
    for(i=0; i<G->V; i++) {
        if(G->sourcenode[ts[i]] == 1) {
            v = ts[i];
            printf("Nodo: %s\n", STgetbyindex(G->table, v).nome);
            for (j = 0; j < G->V; j++)
                dist[j] = MINWT;
            dist[v] = 0;
            for (j = i; j < G->V; j++) {
                w = ts[j];
                if (dist[w] == MINWT)
                    continue;
                for (k = 0; k < G->V; k++) {
                    if (G->madj[w][k] != 0)
                        if (dist[k] == MINWT || dist[w] + G->madj[w][k] > dist[k])
                            dist[k] = dist[w] + G->madj[w][k];
                }
            }
            for (j = 0; j < G->V; j++) {
                if (j != v)
                    printf("- %s distanza massima %d\n", STgetbyindex(G->table, j).nome, dist[j]);
            }
        }
    }
}
