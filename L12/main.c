#include <stdio.h>
#include <stdlib.h>

#include "grafo.h"

int main(){
    FILE *in;
    Graph G;
    in = fopen("grafo1.txt", "r");
    G = GRAPHreadFile(in);
    printf("Grafo di %d nodi e %d archi caricato in memoria.\n", GRAPHgetV(G),GRAPHgetE(G));
    GRAPHtoDAG(G);
    printf("Elenco dei nodi sorgente del DAG: \n");
    DAGprintSource(G);
    DAGlongestPathS(G);
    return 0;
}