#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "st.h"


struct hashtable{
    int n,max;
    Item *v;
};

ST STinit(int max){
    ST table;
    table = malloc(sizeof(*table));
    table->max = max;
    table->n = 0;
    table->v = malloc(max* sizeof(Item));
    return table;
}

void STfree(ST table){
    int i;
    for(i=0; i<table->n; i++)
        itemFree(table->v[i]);
    free(table->v);
    free(table);
}

int STsearchbyname(ST table, char *key){
    int i;
    for(i=0; i<table->n; i++){
        if(strcmp(table->v[i].nome,key) == 0)
            return i;
    }
    return -1;
}

Item STgetbyindex(ST table, int index){
    return table->v[index];
}

void STprint(ST table){
    int i;
    for(i=0; i<table->n; i++)
        printf("%2d | %10s\n",i,table->v[i].nome);
    printf("\n\n");
}

int STinsert(ST table, Item x){
    if(table->n == table->max)
        return -1;
    table->v[table->n++] = x;
    return 1;
}

void swap(Item *v, int n1, int n2){
    Item temp;
    temp  = v[n1];
    v[n1] = v[n2];
    v[n2] = temp;
    return;
}

static int partition(Item *v, int l, int r){
    int i=l-1, j = r;
    Item x;
    x = v[r];
    for( ; ; ){
        while(strcmp(v[++i].nome,x.nome) < 0);
        while(strcmp(v[--j].nome,x.nome) > 0)
            if(j == l)
                break;
        if(i >= j)
            break;
        swap(v,i,j);
    }
    swap(v,i,r);
    return i;
}

static void quicksortR(Item *v, int l, int r){
    int q;
    if(r <= l)
        return;
    q = partition(v,l,r);
    quicksortR(v,l,q-1);
    quicksortR(v,q+1,r);
}

void STsort(ST table){
    int l = 0, r = table->n-1;
    quicksortR(table->v, l, r);
}