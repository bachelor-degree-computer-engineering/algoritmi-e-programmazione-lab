#ifndef GRAFO_ST_H
#define GRAFO_ST_H

#include "Item.h"

typedef struct hashtable *ST;

ST STinit(int n);
void STfree(ST table);
int STsearchbyname(ST table, char *key);
void STprint(ST table);
int STinsert(ST table, Item x);
Item STgetbyindex(ST table, int index);
void STsort(ST table);
#endif //GRAFO_ST_H
